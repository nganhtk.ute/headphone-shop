(function ($) {
  const userId = localStorage.getItem('userId');
  const token = localStorage.getItem('token');

  $.ajax({
    url: `http://localhost:8090/employee/${userId}`,
    type: 'GET',
    headers: {
      "Authorization": `Bearer ${token}`,
    },
    dataType: 'json',
    success: (res) => {
      $('#user-name').text(`${res.username}`)
    },
  });

  $('#header-navbar').html(`
  <ul class="navbar-nav">
     <li class="nav-item">
         <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
     </li>
     <li class="nav-item d-none d-sm-inline-block">
         <a href="#" class="nav-link">Home</a>
     </li>     
  </ul>

<!-- Right navbar links -->
<ul class="navbar-nav ml-auto">
  <!-- Navbar Search -->
  <li class="nav-item">
    <a class="nav-link" data-widget="navbar-search" href="#" role="button">
      <i class="fas fa-search"></i>
    </a>
    <div class="navbar-search-block">
      <form class="form-inline">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search"
            aria-label="Search" />
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
            <button class="btn btn-navbar" type="button" data-widget="navbar-search">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
      </form>
    </div>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link" data-toggle="dropdown" href="#">
      <i class="fa-solid fa-user"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
      <div id="user-name" class="dropdown-item dropdown-header text-left">
        <i class="fa-solid fa-user mr-2"></i>
        <span id="username"></span>
      </div>
      <div class="dropdown-divider"></div>
      <a id="logout-btn" href="logout.html" class="dropdown-item">
        <i class="fa-solid fa-right-from-bracket mr-2"></i>
        Logout
      </a>
    </div>
  </li>
</ul>
    `);
}(jQuery));
