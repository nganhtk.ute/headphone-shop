(($) => {
    // Hàm tải dữ liệu tỉnh vào select
    function loadDataToProvinceSelect(pProvinceList) {
        $('#select-province').html('');

        const defaultOption = $('<option/>');
        defaultOption.prop('value', '');
        defaultOption.prop('text', 'Choose province');
        $('#select-province').append(defaultOption);

        pProvinceList.forEach((bProvince) => {
            const option = $('<option/>');
            option.prop('value', bProvince.id);
            option.prop('text', bProvince.provinceName);
            $('#select-province').append(option);
        });
    }

    // Hàm tải dữ liệu quận vào select
    function loadDataToDistrictSelect(pDistrictList) {
        $('#select-district').html('');

        const defaultOption = $('<option/>');
        defaultOption.prop('value', '');
        defaultOption.prop('text', 'Choose district');
        $('#select-district').append(defaultOption);

        pDistrictList.forEach((bDistrict) => {
            const option = $('<option/>');
            option.prop('value', bDistrict.id);
            option.prop('text', bDistrict.districtName);
            $('#select-district').append(option);
        });
    }

    // Hàm tải dữ liệu phường vào select
    function loadDataToWardSelect(pWardList) {
        $('#select-ward').html('');

        const defaultOption = $('<option/>');
        defaultOption.prop('value', '');
        defaultOption.prop('text', 'Choose ward');
        $('#select-ward').append(defaultOption);

        pWardList.forEach((bWard) => {
            const option = $('<option/>');
            option.prop('value', bWard.id);
            option.prop('text', bWard.wardName);
            $('#select-ward').append(option);
        });
    }

    // Hàm tải dữ liệu đường vào select
    function loadDataToStreetSelect(pStreetList) {
        $('#select-street').html('');

        const defaultOption = $('<option/>');
        defaultOption.prop('value', '');
        defaultOption.prop('text', 'Choose Street');
        $('#select-street').append(defaultOption);

        pStreetList.forEach((bStreet) => {
            const option = $('<option/>');
            option.prop('value', bStreet.id);
            option.prop('text', bStreet.streetName);
            $('#select-street').append(option);
        });
    }

    // Xử lý sự kiện khi province được chọn
    $(document).on('change', '#select-province', (event) => {
        const provinceId = Number(event.target.value);
        $('#select-district').prop('disabled', true);

        $.ajax({
            url: `http://localhost:8090/provinces/${provinceId}/districts`,
            type: 'GET',
            dataType: 'json',
            success: (response) => {
                const districtId = Number($('#select-district').val());

                loadDataToDistrictSelect(response);

                if (response.find((x) => x.id === districtId)) {
                    $('#select-district').val(districtId).trigger('change');
                }

                $('#select-district').prop('disabled', false);
            },
        });
    });

    // Xử lý sự kiện khi district được chọn
    $(document).on('change', '#select-district', (event) => {
        const districtId = Number(event.target.value);
        $('#select-ward').prop('disabled', true);

        $.ajax({
            url: `http://localhost:8090/districts/${districtId}/wards`,
            type: 'GET',
            dataType: 'json',
            success: (response) => {
                const wardId = Number($('#select-ward').val());

                loadDataToWardSelect(response);

                if (response.find((x) => x.id === wardId)) {
                    $('#select-ward').val(wardId).trigger('change');
                }

                $('#select-ward').prop('disabled', false);
            },
        });
    });

    // Xử lý sự kiện khi district được chọn
    $(document).on('change', '#select-district', (event) => {
        const districtId = Number(event.target.value);
        $('#select-street').prop('disabled', true);

        $.ajax({
            url: `http://localhost:8090/districts/${districtId}/streets`,
            type: 'GET',
            dataType: 'json',
            success: (response) => {
                const streetId = Number($('#select-street').val());

                loadDataToStreetSelect(response);

                if (response.find((x) => x.id === streetId)) {
                    $('#select-street').val(streetId).trigger('change');
                }

                $('#select-street').prop('disabled', false);
            },
        });
    });

    $.ajax({
        url: 'http://localhost:8090/provinces/',
        type: 'GET',
        dataType: 'json',
        success: (response) => {
            loadDataToProvinceSelect(response);
        },
    });
})(jQuery);
