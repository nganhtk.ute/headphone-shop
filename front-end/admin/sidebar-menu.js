(function ($) {
  const locationURL = window.location.pathname;

  $('#sidebar-menu').html(`
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
           
            <li class="nav-item ${(locationURL.includes('/customer.html') || locationURL.includes('/customer-report.html')) && 'menu-open'}">
              <a href="#" class="nav-link ${(locationURL.includes('/customer.html') || locationURL.includes('/customer-report.html')) && 'active'}">
                <i class="fa-solid fa-users nav-icon"></i>
                <p>
                  Customer
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="customer.html" class="nav-link ${locationURL.includes('/customer.html') && 'active'}">
                    <p>Customer List</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="customer-report.html" class="nav-link ${locationURL.includes('/customer-report.html') && 'active'}">
                    <p>Customer Report</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="product.html" class="nav-link ${locationURL.includes('/product.html') && 'active'}">
                <i class="fa-solid fa-box-open nav-icon"></i>
                <p>Product</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="productLine.html" class="nav-link ${locationURL.includes('/productLine.html') && 'active'}">
                <i class="fa-solid fa-pallet nav-icon"></i>
                <p>ProductLine</p>
              </a>
            </li>
            <li class="nav-item ${(locationURL.includes('/order.html') || locationURL.includes('/order-report.html') || locationURL.includes('/order-detail.html')) && 'menu-open'}">
              <a href="#" class="nav-link ${(locationURL.includes('/order.html') || locationURL.includes('/order-report.html') || locationURL.includes('/order-detail.html')) && 'active'}">
                <i class="fa-solid fa-clipboard-list nav-icon"></i>
                <p>
                  Order
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="order.html" class="nav-link ${locationURL.includes('/order.html') && 'active'}">
                    <p>Order List</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="order-report.html" class="nav-link ${locationURL.includes('/order-report.html') && 'active'}">
                    <p>Order Report</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="order-detail.html" class="nav-link ${locationURL.includes('/order-detail.html') && 'active'}">
                    <p>Order Detail</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a href="employee.html" class="nav-link ${locationURL.includes('/employee.html') && 'active'}">
                <i class="fa-solid fa-user nav-icon"></i>
                <p>Employee</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="review.html" class="nav-link ${locationURL.includes('/review.html') && 'active'}">
                  <i class="fa-solid fa-comments nav-icon"></i>
                  <p>Review</p>
              </a>
            </li>
             <li class="nav-item">
              <a href="discount.html" class="nav-link ${locationURL.includes('/discount.html') && 'active'}">
                  <i class="fa-solid fa-percent nav-icon"></i>
                  <p>Discount</p>
              </a>
            </li>
            <li class="nav-item ${(locationURL.includes('/provinces.html') || locationURL.includes('/districts.html') || locationURL.includes('/wards.html') || locationURL.includes('/streets.html')) && 'menu-open'}">
              <a href="#" class="nav-link ${(locationURL.includes('/provinces.html') || locationURL.includes('/districts.html') || locationURL.includes('/wards.html') || locationURL.includes('/streets.html')) && 'active'}">
                <i class="nav-icon fa-solid fa-map-location"></i>
                <p>
                  Location
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="provinces.html" class="nav-link ${locationURL.includes('/provinces.html') && 'active'}">
                    <p>Province</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="districts.html" class="nav-link ${locationURL.includes('/districts.html') && 'active'}">
                    <p>District</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="wards.html" class="nav-link ${locationURL.includes('/wards.html') && 'active'}">
                    <p>Ward</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="streets.html" class="nav-link ${locationURL.includes('/streets.html') && 'active'}">
                    <p>Street</p>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
    `);
}(jQuery));
