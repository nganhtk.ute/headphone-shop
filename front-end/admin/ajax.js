(function ($) {
    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {

        const token = localStorage.getItem('token');

        if (token && !options.url.includes('datatables')) {
            jqXHR.setRequestHeader('Authorization', `Bearer ${token}`);
        }

        if (!options.url.includes('http')) {
            options.url = `http://localhost:8090${options.url}`;
        }
    });
}(jQuery));
