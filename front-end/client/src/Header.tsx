import React, { useEffect } from 'react';
import { MdNotifications, MdAccountCircle, MdShoppingCart } from "react-icons/md";
import { FaHeadphonesAlt } from "react-icons/fa";
import './Grid.css';
import './Header.css';

import { useState } from "react";
import { OrderDetail } from './Products';
import { Link, useNavigate } from 'react-router-dom';
import { AiOutlineReload } from 'react-icons/ai';
import { showToastErrorMessage } from './ChangePassword';
import { ToastContainer } from 'react-toastify';
import { Order } from './Payment';
import { Product } from './Slider';

export interface Quantity {
    quantity: number
}
interface DisplayOrderCartProps {
    orderDetails: OrderDetail[]
}

// Hàm hiển thị giỏ hàng
function DisplayOrderCart(props: DisplayOrderCartProps) {
    const { orderDetails } = props;

    // Xóa item
    function removeItem(id: number) {
        const orderList = orderDetails.filter((orderDetail) => orderDetail.product.id !== id);

        localStorage.setItem("orders", JSON.stringify(orderList));
        window.dispatchEvent(new Event("storage"));
    }

    // Hàm tăng số lượng
    const increaseQuantity = (id: number) => {
        const orderList = orderDetails.map((orderDetail) => {
            if (orderDetail.product.id === id) {
                orderDetail.quantity += 1;
            }

            return orderDetail;
        });

        localStorage.setItem("orders", JSON.stringify(orderList));
        window.dispatchEvent(new Event("storage"));
    }

    // Hàm giảm số lượng
    const decreaseQuantity = (id: number) => {
        const orderList = orderDetails.map((orderDetail) => {
            if (orderDetail.product.id === id && orderDetail.quantity >= 2) {
                orderDetail.quantity -= 1;
            } else if (orderDetail.product.id === id && orderDetail.quantity === 1) {
                removeItem(id);
            }
            return orderDetail;
        });
        localStorage.setItem("orders", JSON.stringify(orderList));
        window.dispatchEvent(new Event("storage"));
    }

    if (orderDetails.length !== 0) {
        return (
            <>
                <ul className="header__cart-list-item">
                    {
                        orderDetails.map((orderDetail) => (<DisplayOrderItem key={orderDetail.product.id} orderDetail={orderDetail} increaseQuantity={increaseQuantity} decreaseQuantity={decreaseQuantity} removeItem={removeItem} />))
                    }

                </ul>
                <div style={{ padding: "15px" }}>
                    <div className="" style={{ marginBottom: "20px", display: "flex", alignItems: "center", justifyContent: "space-between" }}>
                        <div style={{ color: "#ccc" }} className="l-4">
                            Total
                        </div>
                        <div className="" style={{ display: "flex", justifyContent: "flex-end", }}>
                            <TotalPrice orderDetails={orderDetails} />
                        </div>
                    </div>
                    <Link style={{ display: "flex", justifyContent: "center", }} to="/cart">
                        <button style={{ fontSize: "1.1rem" }} className="header__cart-view-cart">Payment
                        </button>
                    </Link>
                </div>
            </>
        )
    } else {
        return (
            <div className='header__cart-list-empty-msg'>
                <p className="header__cart-list-no-cart-msg">
                    Your cart is empty
                </p>
            </div>
        )
    }
}

// Hàm hiển thị số lượng lên input
function InputQuantity(props: Quantity) {
    const { quantity } = props;

    return (
        <input className="header__cart-item-qty" type="text" readOnly value={quantity} />
    )
}

interface TotalPriceProps {
    orderDetails: OrderDetail[];
}
// Hàm tính totalPrice
function TotalPrice(props: TotalPriceProps) {
    const { orderDetails } = props;

    const result = orderDetails.reduce((previousValue, currentValue) => (
        previousValue = previousValue + (currentValue.quantity * currentValue.sellPrice)
    ), 0)

    return (
        <b style={{ color: "#fe9614s" }}>${result}</b>
    )
}

interface DisplayOrderItemProps {
    orderDetail: OrderDetail;
    increaseQuantity: (id: number) => void;
    decreaseQuantity: (id: number) => void;
    removeItem: (id: number) => void;
}

// Hàm hiển thị đơn hàng
function DisplayOrderItem(props: DisplayOrderItemProps) {
    const { orderDetail, increaseQuantity, decreaseQuantity, removeItem } = props;
    const { product, quantity } = orderDetail;
    const { id, productName, color, sellPrice, photos } = product;

    return (
        <li className="header__cart-item">
            <img className="header__cart-img" src={photos[0].url} alt="" />
            <div className="header__cart-item-info">
                <div className="row">
                    <div className="header__cart-item-head l-9 m-9 c-9">
                        <h5 className="header__cart-item-name">{productName}</h5>
                    </div>
                    <div onClick={() => removeItem(id)} className='l-3 m-3 c-3 header__cart-item-remove' style={{ display: "flex", justifyContent: "flex-end" }}>
                        Remove
                    </div>
                </div>
                <div className="header__cart-item-body">
                    <span className="header__cart-item-description">{color.name}</span>
                </div>
                <div>
                    <div className="header__cart-item-price-wrap">
                        <div className='row'>
                            <div className="l-6m-6 c-6 header__cart-item-qnt">
                                Quantity
                            </div>
                            <div className="l-6m-6 c-6 header__cart-item-price" style={{ display: "flex", justifyContent: "flex-end" }}>
                                ${sellPrice}
                            </div>
                            <div>
                                <div className='header__cart-item-qty-wrap'>
                                    <button onClick={() => decreaseQuantity(id)} className='header__body-buttonMinus'>-</button>
                                    <InputQuantity quantity={quantity} />
                                    <button className='header__body-buttonPlus' onClick={() => increaseQuantity(id)}>+</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    )
}

function Header() {
    const navigate = useNavigate();

    const token = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');
    const [orders, setOrders] = useState<OrderDetail[]>([]);

    const [totalPrice, setTotalPrice] = useState(0);

    useEffect(() => {

        function checkOrder() {
            const orders = JSON.parse(localStorage.getItem('orders') || '[]');
            setOrders(orders);
        }

        window.addEventListener('storage', checkOrder);

        checkOrder();
    }, []);

    //Hàm gọi API logout
    async function callApiLogout() {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/log-out`, {
                method: "POST",
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem('token')}`,
                },
            });
            localStorage.removeItem('token');
            localStorage.removeItem('username');
            localStorage.removeItem('orders');
            window.localStorage.removeItem('orders');
            navigate("/login")
            // window.location.reload();
        } catch (error) {

            const errorString = JSON.stringify(error);
            showToastErrorMessage(errorString);
        }

    }

    // Hàm hiển thị menu user
    function DisplayUserMenu() {
        if (token) {
            return (
                <ul className="header__navbar-user-menu">
                    <li className="header__navbar-user-item">
                        <Link to={`/profile?id=${userId}`}>My account</Link>
                    </li>
                    <li className="header__navbar-user-item header__navbar-user-item--separate">
                        <a onClick={callApiLogout} href="#">Logout</a>

                    </li>

                </ul>
            )
        } else {
            return (
                <ul className="header__navbar-user-menu">
                    <li className="header__navbar-user-item header__navbar-user-item--separate">
                        <Link to="/register">Register</Link>
                    </li>
                    <li className="header__navbar-user-item header__navbar-user-item--separate">
                        <Link to="/login">Login</Link>
                    </li>
                </ul>
            )
        }

    }

    return (
        <header className='header'>
            <div className="grid wide">
                <div className="row">
                    <nav className='l-12 m-12 c-12'>
                        <div className="row header__navbar ">
                            <div className="l-1 m-1 c-0"></div>
                            <div className='l-10 m-10 c-12'>
                                <div className="header__navbar-item-wrap">
                                    <Link to={'/'}>
                                        <img src="/shop/headphone-logo.png" className="header__logo hide-on-mobile-tablet hide-on-mobile" alt="" />
                                    </Link>
                                    <ul className='header__navbar-list header__listIcon'>
                                        <li className='header__navbar-item header__btn' >
                                            <Link to="/products" className="header__navbar-item-link">
                                                <FaHeadphonesAlt />
                                            </Link>
                                        </li>
                                        <li className='header__navbar-item header__navbar-user header__btn'>
                                            <a href="#" className="header__navbar-item-link">
                                                <MdAccountCircle />
                                            </a>
                                            <DisplayUserMenu />
                                        </li>
                                        <li className='header__navbar-item header__btn'>
                                            <div className="header__cart-wrap">
                                                <Link to='/cart'>
                                                    <MdShoppingCart className="header__navbar-item-link" />
                                                </Link>
                                                <span className="header__cart-notice">
                                                    {orders.length}
                                                </span>
                                            </div>
                                            <div className="header__cart-list">
                                                <DisplayOrderCart orderDetails={orders} />
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="l-1 m-1 c-0"></div>
                        </div>
                    </nav>
                </div>
            </div >
        </header >

    )
}
export default Header;