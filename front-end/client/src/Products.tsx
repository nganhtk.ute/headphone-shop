import React, { useEffect, useState } from 'react';
import MultiRangeSlider from "./component/multiRangeSlider/MultiRangeSlider";
import { MdKeyboardDoubleArrowRight, MdSearch } from "react-icons/md";
import './Products.css';
import './Grid.css';
import './ProductItem.css';

import { Product } from './Slider';
import ProductItem from './ProductItem';
import { number } from 'yup';
import { Link } from 'react-router-dom';

export interface OrderDetail {
  id?: number;
  product: Product;
  quantity: number;
  sellPrice: number;
  reviewStatus: boolean;
}

function Products() {

  const [productList, setProductList] = useState<Product[]>([]);
  const [pageNum, setPageNum] = useState(0);
  const [category, setCategory] = useState(0);
  const [brand, setBrand] = useState(0);
  const [color, setColor] = useState(0);
  const [priceMin, setPriceMin] = useState('');
  const [priceMax, setPriceMax] = useState('');
  const [totalPages, setTotalPages] = useState(0);
  const [keyword, setKeyword] = useState<string>('');

  const [btnPageOne, setBtnPageOne] = useState('pagination_active');
  const [btnPageTwo, setBtnPageTwo] = useState('');
  const [btnPageThree, setBtnPageThree] = useState('');
  const [btnPageFour, setBtnPageFour] = useState('');
  const [btnPageFive, setBtnPageFive] = useState('');

  // Hàm lấy sản phẩm theo filter
  // useEffect(() => {
  //   const searchParams = new URLSearchParams();

  //   searchParams.append('page', String(pageNum));
  //   searchParams.append('size', String(8));

  //   if (brand !== 0) {
  //     searchParams.append('brand', String(brand));
  //   }

  //   if (color !== 0) {
  //     searchParams.append('color', String(color));
  //   }

  //   if (category !== 0) {
  //     searchParams.append('category', String(category));
  //   }

  //   if (keyword !== "") {
  //     searchParams.append('keyword', keyword);
  //   }

  //   searchParams.append('priceMin', String(priceMin));
  //   searchParams.append('priceMax', String(priceMax));

  //   fetch(`${process.env.REACT_APP_API_URL}/product/paging1?${searchParams}`)
  //     .then((response) => {
  //       if (!response.ok) {
  //         throw new Error(
  //           `This is an HTTP error: The status is ${response.status}`
  //         );
  //       }
  //       return response.json();
  //     })
  //     .then((actualData) => {
  //       setProductList(actualData.content)
  //       setTotalPages(actualData.totalPages);
  //     })
  // }, []);

  useEffect(() => {
    callApiGetProduct(pageNum);
  }, [pageNum])
  //pageNum, category, brand, color, priceMin, priceMax, keyword
  const [style, setStyle] = useState('sidebar__filter');

  function callApiGetProduct(targetPageNum = pageNum) {
    const searchParams = new URLSearchParams();

    searchParams.append('page', String(targetPageNum));
    searchParams.append('size', String(8));

    if (brand !== 0) {
      searchParams.append('brand', String(brand));
    }

    if (color !== 0) {
      searchParams.append('color', String(color));
    }

    if (category !== 0) {
      searchParams.append('category', String(category));
    }

    if (keyword !== "") {
      searchParams.append('keyword', keyword);
    }

    searchParams.append('priceMin', String(priceMin));
    searchParams.append('priceMax', String(priceMax));


    fetch(`${process.env.REACT_APP_API_URL}/product/paging1?${searchParams}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setProductList(actualData.content)
        setTotalPages(actualData.totalPages);
      })
  }

  // Hàm hiển thị nút previous
  function PreviousButton() {
    return (
      <button disabled={pageNum === 0 || productList.length === 0} onClick={() => setPageNum(pageNum - 1)}>&laquo;</button>
    )
  }

  // Hàm hiển thị nút next
  function NextButton() {
    return (
      <button disabled={pageNum === totalPages - 1 || productList.length === 0} onClick={() => setPageNum(pageNum + 1)}>&raquo;</button>
    )
  }

  function NumberPagination() {
    let arr = []
    for (let index = 0; index < totalPages; index++) {
      arr.push(
        <button className={pageNum === index ? 'pagination_active' : ''} onClick={() => {
          setPageNum(index);
          window.scrollTo({ top: 0, behavior: 'smooth' });
        }}>
          {index + 1}
        </button>)
    }
    return (<>{arr}</>);
  }

  return (
    <>
      <div className={style}>
      </div>
      <div className="product__nav">
        <div className="row product__nav-wrap">
          <Link className="nav-home" to="/">
            <b>Home</b>
          </Link>
          <div className="detail-nav-icon">
            <MdKeyboardDoubleArrowRight />
          </div>
          <Link to="/products" className="nav-all-product">
            <b>All Products</b>
          </Link>
        </div>
      </div>
      <div className='product_filter row'>
        <div className='l-4 m-5 c-12  product_filter-item-wrap'>
          <div className='product_filter-item-categories'>
            <select onChange={e => setCategory(Number(e.target.value))} name="" id="">
              <option value="">Category</option>
              <option value="13">Wireless</option>
              <option value="14">In-ear headphone</option>
              <option value="15">Over-ear headphone</option>
              <option value="16">Sport headphone</option>
            </select>
          </div>
          <div className='product_filter-item'>
            <select onChange={e => { setBrand(Number(e.target.value)) }} name="" id="">
              <option value="">Brand</option>
              <option value="8">JBL</option>
              <option value="9">Beat</option>
              <option value="10">Logitech</option>
              <option value="11">Samsung</option>
              <option value="12">Sony</option>
            </select>
          </div>
          <div className='product_filter-item'>
            <select onChange={e => setColor(Number(e.target.value))} name="" id="">
              <option value="">Color</option>
              <option value="1">Black</option>
              <option value="2">White</option>
              <option value="3">Pink</option>
              <option value="4">Blue</option>
              <option value="5">Grey</option>
              <option value="6">Red</option>
              <option value="7">Yellow</option>
            </select>
          </div>
        </div>
        <div className='l-3 m-6 c-12  product_filter-item-priceRange'>
          <MultiRangeSlider
            min={0}
            max={1000}
            onChange={({ min, max }) => { setPriceMin(min); setPriceMax(max) }}
          />
        </div>
        <div className='l-4 m-4 c-12 product_filter-item-search'>
          Search &nbsp;
          <input onChange={event => setKeyword(event.target.value)} value={keyword} className="mainLoginInput" type="text" />
        </div>
        <div className="l-1 m-12 c-12">
          <button onClick={() => callApiGetProduct(0)} className='filter-button'>
            Filter
          </button>
        </div>
      </div >
      <div className='productList-container'>
        <div className="grid">
          <div className="row productItem-container">
            <div className="productItem-container l-12 m-12 c-12">
              <div className="row" id="productList-wrap">
                {(productList.length !== 0) ? productList.map(item => (
                  <ProductItem key={item.id} product={item} />)) : <p className='p-style'>Could not find your request</p>
                }
              </div>
            </div>
          </div>
          <div className="pagination">
            <PreviousButton />
            <NumberPagination />

            <NextButton />
          </div>
        </div>
      </div >

    </>
  );
}

export default Products;

