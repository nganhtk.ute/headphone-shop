import React, { useEffect, useState } from 'react';
import { MdKeyboardDoubleArrowRight } from "react-icons/md";
import './Grid.css';
import './Payment.css';
import './OrderStatus.css';
import { DisplayOrder, Order } from './Payment';
import { Link } from 'react-router-dom';


function OrderStatus() {

    const [order, setOrder] = useState<Order>(null);
    const searchParams = new URLSearchParams(window.location.search);
    const idOrder = Number(searchParams.get('id'));
    const percent = Number(searchParams.get('percent'))
    const totalPrice = Number(searchParams.get('totalPrice'))


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/order/${idOrder}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(
                        `This is an HTTP error: The status is ${response.status}`
                    );
                }
                return response.json();
            })
            .then((actualData) => {
                setOrder(actualData);
            })
    }, []);

    //Tính tổng số tiền
    function TotalPrice() {
        return (
            <div className='totalPrice_wrap'>
                <p>Total Price: </p>
                <div>
                    <span><b>$</b></span>
                    <span>
                        <b id="totalPrice">{totalPrice}</b>
                    </span>
                </div>
            </div>
        )
    }

    // Hàm tính tổng cuối cùng
    function FinalPrice() {
        return (
            <b>
                {totalPrice - (percent * totalPrice) / 100}
            </b>
        )
    }
    return (
        <>
            <div className='orderStatus__container'>
                <div className="checkout__nav">
                    <div className="grid">
                        <div className="row">
                            <div className="l-7 m-12 c-12">
                                <div className="row checkout__nav-row">
                                    <Link style={{ textDecoration: "none", margin: "0", display: "flex", justifyContent: "flex-end", color: "black" }} to="/" className="l-1 m-2 c-2 checkout_nav-cart">
                                        <b>Cart</b>
                                    </Link>
                                    <div style={{ display: "flex", justifyContent: "center" }} className="l-1 m-1 c-2 detail-nav-icon"><MdKeyboardDoubleArrowRight />
                                    </div>
                                    <div style={{ display: "flex", justifyContent: "flex-start" }} className="l-3 m-4 c-4 cart_nav-all-product"><b>Order Status</b></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='orderStatus_container'>
                    <div className="orderStatus_confirm">
                        <h3>Order Success</h3>
                        <div className="orderStatus_wrap border">
                            <p>Thank you. Your order has been received. We will call you back immediately!
                            </p>
                            <div>
                                <span>Order Code: </span><span>{order?.id}</span>
                            </div>
                            <div>
                                <span>Order Date: </span><span>{new Date(order?.orderDate).toLocaleString()}</span>
                            </div>
                        </div>
                        <div className="orderStatus_wrap border">
                            <h3>Receiver info</h3>
                            <div>
                                <span>Fullname: </span><span>{order?.receiverName}</span>
                            </div>
                            <div>
                                <span>Phone: </span><span>{order?.receiverPhone}</span>
                            </div>
                            <div>
                                <span>Address: </span><span>{order?.receiverAddress}</span>
                            </div>
                        </div>
                        <div className="orderStatus_wrap border">
                            <h3>Order Detail</h3>
                            {order?.orderDetails.map(item => (
                                <DisplayOrder key={item.id} {...item} />
                            ))}
                            <div>
                                <div>
                                    <TotalPrice />
                                </div>
                                <div className='discount-wrap'>
                                    <p>Discount: </p>
                                    <div >
                                        <span><b>$</b></span>
                                        <span><b id="discount">{(percent * totalPrice) / 100}</b></span>
                                    </div>
                                </div>
                                <div className='total-wrap'>
                                    <p>Total: </p>
                                    <div>
                                        <span><b>$</b></span>
                                        <span>
                                            <FinalPrice />
                                        </span>
                                    </div>
                                </div>
                                <div className='paymentMethod-wrap'>
                                    <p>Payment method: </p>
                                    <div>
                                        <span><b>{order?.paymentMethod}</b></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div >
        </>
    )
}

export default OrderStatus;