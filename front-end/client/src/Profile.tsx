import React, { useEffect, useState } from 'react';
import { MdKeyboardDoubleArrowRight } from "react-icons/md";
import { GrDocumentText } from "react-icons/gr";
import { FaRegUser } from "react-icons/fa";
import { FaLock } from "react-icons/fa";
import { FiLock } from "react-icons/fi";
import './Grid.css';
import './Payment.css';
import './Profile.css';
import { OrderDetail } from './Products';
import { Customer, District, Province, Street, Ward, showToastSuccessMessage } from './ProductItem';
import { ValidationError, object, string } from 'yup';
import { Input } from './Register';
import { Link } from 'react-router-dom';
import { showToastErrorMessage } from './ChangePassword';
import { ToastContainer } from 'react-toastify';
import { DistrictProps, ProvinceProps, StreetProps, WardProps } from './Payment';
import Locations from './Locations';

function Profile() {
    const [username, setUsername] = useState('');
    const [fullname, setFullname] = useState('');
    const [email, setEmail] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState('');

    const [customerProvince, setCustomerProvince] = useState<string>('');
    const [customerDistrict, setCustomerDistrict] = useState<string>('');
    const [customerWard, setCustomerWard] = useState<string>('');
    const [customerStreet, setCustomerStreet] = useState<string>('');

    const [messageDistrict, setMessageDistrict] = useState('');
    const [messageProvince, setMessageProvince] = useState('');
    const [messageWard, setMessageWard] = useState('');
    const [messageStreet, setMessageStreet] = useState('');

    const userId = localStorage.getItem("userId");
    const [customer, setCustomer] = useState<Customer>(null);



    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/customer/${userId}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(
                        `This is an HTTP error: The status is ${response.status}`
                    );
                }
                return response.json();
            })
            .then((customerData) => {
                setCustomer(customerData);
                setUsername(customerData.username);
                setFullname(customerData.fullname ? customerData.fullname : "");
                setEmail(customerData.email ? customerData.email : "");
                setPhone(customerData.phone);
                setCustomerProvince(customerData?.province?.id);
                setCustomerDistrict(customerData?.district?.id);
                setCustomerWard(customerData?.ward?.id);
                setCustomerStreet(customerData?.street?.id);
            })
    }, []);

    async function callApiUpdateCustomer() {
        const customerData = {
            fullname: fullname,
            phone: phone,
            email: email,
            province: {
                id: Number(customerProvince),
            },
            district: {
                id: Number(customerDistrict),
            },
            ward: {
                id: Number(customerWard),
            },
            street: {
                id: Number(customerStreet),
            },
        }
        try {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/customer/me`, {
                method: "PUT",
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem('token')}`,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(customerData),
            });
            // debugger
            const result = await response.json();
            showToastSuccessMessage('Save successful')
        } catch (error) {
            const errorString = JSON.stringify(error);
            showToastErrorMessage(errorString);
        }
    }

    return (
        <>
            <div className='profile__container'>
                <div className="profile__nav">
                    <div className="row profile__nav-wrap">
                        <Link to="/" className="checkout_nav-cart">
                            <b>Home</b>
                        </Link>
                        <div className="detail-nav-icon">
                            <MdKeyboardDoubleArrowRight />
                        </div>
                        <div className="cart_nav-all-product">
                            <b>My profile</b>
                        </div>
                    </div>
                </div>

                <div className='profile_container'>
                    <div className="row ">
                        <div className="l-2 m-3 c-12 profile_container-sidebar">
                            <div className="leftSide_menu">
                                <div className="leftSide_menu-header">
                                    {username}
                                </div>
                                <div className="leftSide_menu-body">
                                    <div className='leftSide_menu-body-item'>
                                        <FaRegUser />
                                        <Link style={{ color: "#fe9614" }} to="/profile">My account</Link>
                                    </div>
                                    <div className='leftSide_menu-body-item'>
                                        <GrDocumentText />
                                        <Link to={`/my-order?id=${userId}`}>My orders</Link>
                                    </div>
                                    <div className='leftSide_menu-body-item'>
                                        <FiLock />
                                        <Link to={`/change-password?id=${userId}`}>Change password</Link>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="l-10 m-9 c-12 profile_container-content">
                            <div className="profile_content-wrap">
                                <div className="profile_content-header">
                                    <h3>My profile</h3>
                                </div>
                                <div className="profile_content-body">
                                    <div className='row profile_content-body-item'>
                                        <div className="l-2 profile_content-body-label">
                                            Username:
                                        </div>
                                        <div className="l-10 profile_content-body-input">
                                            <input id='username-input' value={username} style={{ fontFamily: "sans-serif" }} type="text" disabled />
                                        </div>
                                    </div>
                                    <div className='row profile_content-body-item'>
                                        <div className="l-2 profile_content-body-label">
                                            Fullname:
                                        </div>
                                        <div className="l-10 profile_content-body-input">
                                            <input value={fullname} onChange={e => setFullname(e.target.value)} type="text" style={{ fontFamily: "sans-serif" }} />
                                        </div>
                                    </div>
                                    <div className='row profile_content-body-item'>
                                        <div className="l-2 profile_content-body-label">
                                            Email:
                                        </div>
                                        <div className="l-10 profile_content-body-input">
                                            <input onChange={e => setEmail(e.target.value)} style={{ fontFamily: "sans-serif" }} value={email} type="text" />
                                        </div>
                                    </div>
                                    <div className='row profile_content-body-item'>
                                        <div className="l-2 profile_content-body-label">
                                            Phone:
                                        </div>
                                        <div className="l-10 profile_content-body-input">
                                            <input onChange={e => setPhone(e.target.value)} style={{ fontFamily: "sans-serif" }} type="text" value={phone} />
                                        </div>
                                    </div>
                                    <div className='row profile_content-body-item'>
                                        <div className="l-2 profile_content-body-label">
                                            Address:
                                        </div>
                                        <div className="l-6">
                                            <Locations
                                                receiverProvince={customerProvince} setReceiverProvince={setCustomerProvince} messageProvince={messageProvince}

                                                receiverDistrict={customerDistrict}
                                                setReceiverDistrict={setCustomerDistrict} messageDistrict={messageDistrict}

                                                receiverWard={customerWard}
                                                setReceiverWard={setCustomerWard}
                                                messageWard={messageWard}

                                                receiverStreet={customerStreet}
                                                setReceiverStreet={setCustomerStreet} messageStreet={messageStreet}
                                            />
                                        </div>
                                    </div>

                                    <div className='row profile_save-btn'>
                                        <div className="l-2"></div>
                                        <div className="l-10">
                                            <button onClick={callApiUpdateCustomer}>Save
                                            </button>
                                            <ToastContainer />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </>
    )
}

export default Profile;