import React, { useEffect, useState } from 'react';
import { MdKeyboardDoubleArrowRight } from "react-icons/md";
import { GrDocumentText } from "react-icons/gr";
import { FaRegUser } from "react-icons/fa";
import { FiLock } from "react-icons/fi";
import './Grid.css';
import './Payment.css';
import './Profile.css';
import { OrderDetail } from './Products';
import { Customer, showToastSuccessMessage } from './ProductItem';
import { ValidationError, object, string } from 'yup';
import { Input } from './Register';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { error } from 'console';

export interface User {
    username: string;
    password: string;
}

export const showToastErrorMessage = (props: string) => {
    toast.error(`${props}`, {
        position: toast.POSITION.TOP_CENTER,
    });
};


export const showToastWarningMessage = (props: string) => {
    toast.warning(`${props}`, {
        position: toast.POSITION.TOP_CENTER,
    });
};

function ChangePassword() {
    const [newPassword, setNewPassword] = useState('');
    const [oldPassword, setOldPassword] = useState('');
    const [username, setUsername] = useState('');

    const searchParams = new URLSearchParams(window.location.search);
    const userId = Number(searchParams.get('id'));
    const [user, setUser] = useState<User>();

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/user/${userId}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(
                        `This is an HTTP error: The status is ${response.status}`
                    );
                }
                return response.json();
            })
            .then((actualData) => {
                setUser(actualData);
                setUsername(actualData.username);
            })
    }, []);

    async function callApiUpdatePassword() {
        const token = localStorage.getItem('token');
        const inputData = {
            oldPassword: oldPassword,
            newPassword: newPassword,
        }
        try {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/change-password`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${token}`,
                },
                body: JSON.stringify(inputData),
            });
            if (response.status === 400) {
                showToastErrorMessage('Old Password incorrect')

            } else if (response.status === 200) {
                showToastSuccessMessage('Change password successful')
            }
            setNewPassword('');
            setOldPassword('');
        } catch (error) {
            const errorString = JSON.stringify(error);
            showToastErrorMessage(errorString);

        }
    }

    return (
        <>
            <div className='profile__container'>
                <div className="profile__nav">
                    <div className="row profile__nav-wrap">
                        <Link to="/" className="checkout_nav-cart">
                            <b>Home</b>
                        </Link>
                        <div className="detail-nav-icon">
                            <MdKeyboardDoubleArrowRight />
                        </div>
                        <div className="cart_nav-my-profile">
                            <b>My profile</b>
                        </div>
                    </div>
                </div>
                <div className='profile_container'>
                    <div className="row ">
                        <div className="l-2 m-2 c-12 profile_container-sidebar">
                            <div className="leftSide_menu">
                                <div className="leftSide_menu-header">
                                    {username}
                                </div>
                                <div className="leftSide_menu-body">
                                    <div className='leftSide_menu-body-item'>
                                        <FaRegUser />
                                        <Link to={`/profile?id=${userId}`}>My account
                                        </Link>
                                    </div>
                                    <div className='leftSide_menu-body-item'>
                                        <GrDocumentText />
                                        <Link to={`/my-order?id=${userId}`}>My orders
                                        </Link>
                                    </div>
                                    <div className='leftSide_menu-body-item'>
                                        <FiLock />
                                        <Link style={{ color: "#fe9614" }} to={`/change-password?id=${userId}`}>Change password
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="l-10 m-10 c-12 profile_container-content">
                            <div className="profile_content-wrap">
                                <div className="profile_content-header">
                                    <h3>Change Password</h3>
                                </div>
                                <div className="profile_content-body">
                                    <div className='row profile_content-body-item'>
                                        <div className="l-2 profile_content-body-label">
                                            Old Password:
                                        </div>
                                        <div className="l-10 profile_content-body-input">
                                            <input onChange={e => setOldPassword(e.target.value)} value={oldPassword} type="password" />
                                        </div>
                                    </div>
                                    <div className='row profile_content-body-item'>
                                        <div className="l-2 profile_content-body-label">
                                            New password:
                                        </div>
                                        <div className="l-10 profile_content-body-input">
                                            <input onChange={e => setNewPassword(e.target.value)} value={newPassword} type="password" />
                                        </div>
                                    </div>
                                    <div className='row profile_save-btn'>
                                        <div className="l-2"></div>
                                        <div className="l-10">
                                            <button onClick={callApiUpdatePassword}>Save</button>
                                            <ToastContainer />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        </>
    )
}

export default ChangePassword;