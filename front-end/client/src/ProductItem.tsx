import React from 'react';
import './Home.css';
import { Product } from './Slider';
import { OrderDetail } from './Products';
import { Link, useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export interface Province {
    id: number;
    provinceName?: string;
    code?: string;
}

export interface District {
    id: number;
    districtName?: string;
    prefix?: string;
}

export interface Ward {
    id: number;
    wardName?: string;
    prefix?: string;
}
export interface Street {
    id: number;
    streetName?: string;
    prefix?: string;
}
export interface Customer {
    id: number;
    password?: string;
    fullname?: string;
    phone?: string;
    email?: string;
    province?: Province;
    district?: District;
    ward?: Ward;
    street?: Street;
    username?: string;
}

export interface ProductItemProps {
    product: Product
}

export const showToastSuccessMessage = (props: string) => {
    toast.success(`${props}`, {
        position: toast.POSITION.TOP_CENTER,
    });
};


function AddToCart(product: Product) {
    const navigate = useNavigate();

    //Hàm lưu orderDetail vào localStorage
    function saveLocalStorage(product: Product) {

        const token = localStorage.getItem('token');

        const orderList: OrderDetail[] = JSON.parse(localStorage.getItem('orders') || '[]');

        if (token) {
            const index = orderList.findIndex(orderDetail =>
                orderDetail.product.id === product.id
            );
            if (index === -1) {
                const orderDetail: OrderDetail = {
                    product: product,
                    quantity: 1,
                    sellPrice: product.sellPrice,
                    reviewStatus: false
                }
                orderList.push(orderDetail);
            } else {
                orderList[index].quantity += 1;
            }
            showToastSuccessMessage('Product has been added to your cart')
            localStorage.setItem("orders", JSON.stringify(orderList));
            window.dispatchEvent(new Event("storage"));
        } else {
            navigate("/login")
        }
    }

    if (product.quantityInStock === 0) {
        return (
            <button disabled style={{ backgroundColor: "#ccc", border: "#ccc" }} className='productItem-addCart'>
                Add to cart
            </button>
        )
    } else {
        return (
            <>
                <button onClick={() => {
                    saveLocalStorage(product)
                }} className='productItem-addCart'>
                    Add to cart
                </button>
                <ToastContainer />
            </>


        )
    }
}

function ImageItem(product: Product) {
    if (product.quantityInStock === 0) {
        return (
            <div className='productItem-img_wrap'>
                <div className='productItem-img_soldOut'>Sold out</div>
                <img className='productItem-img' src={product.photos[0]?.url} alt="" />
            </div>
        )
    } else {
        return (
            <div>
                <img className='productItem-img' src={product.photos[0]?.url} alt="" />
            </div>

        )
    }
}

function ProductItem(props: ProductItemProps) {
    const { product } = props;
    const { id, originalPrice, productCode, productGuarantee, productDescription, productName, quantityInStock, rate, brand, category, color, sellPrice, statusPrice, photos } = product;

    return (
        <div data-id={`${id}`} className='productItem-wrap l-3 m-6 c-6'>
            <Link className='productItem-img-a' to={`/Detail?id=${id}`}>
                <ImageItem {...product} />
                <div className='productItem-title'>
                    <h4>
                        {productName}
                    </h4>
                </div>
                <div className='productItem-price'>
                    <span className='productItem-oldPrice'>${originalPrice}&nbsp;</span>
                    <span className='productItem-price'>${sellPrice}</span>
                </div>
            </Link>
            <AddToCart {...product} />
        </div >
    )
}
export default ProductItem;