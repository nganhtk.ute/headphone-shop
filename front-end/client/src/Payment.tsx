import React, { ChangeEventHandler, useEffect, useState } from 'react';
import { MdKeyboardDoubleArrowRight } from "react-icons/md";
import './Grid.css';
import './Payment.css';
import { OrderDetail } from './Products';
import { Customer, District, Province, Street, Ward, showToastSuccessMessage } from './ProductItem';
import { ValidationError, number, object, string } from 'yup';
import { Input } from './Register';
import { Link, useNavigate } from 'react-router-dom';
import { showToastErrorMessage, showToastWarningMessage } from './ChangePassword';
import { ToastContainer } from 'react-toastify';
import Locations from './Locations';


export interface Order {
    id?: number;
    customer: Customer;
    receiverPhone: string;
    receiverName: string;
    receiverAddress?: string;
    orderDate: string;
    shippedDate: string;
    status: string;
    note: string;
    paymentMethod: string;
    orderDetails: OrderDetail[];
    province: Province;
    district: District;
    ward: Ward;
    street: Street;
}
export interface ProvinceProps {
    id: number;
    provinceName: string | undefined;
}
export interface DistrictProps {
    id: number;
    districtName: string | undefined;
}
export interface WardProps {
    id: number;
    wardName: string | undefined;
}
export interface StreetProps {
    id: number;
    streetName: string | undefined;
}

export interface SelectProps {
    message: string;
    id: string;
    name: string;
    className: string;
    placeholder: string;
    type: string;
    onChange: ChangeEventHandler<HTMLSelectElement>;
    value: string;
}

// Hàm hiển thị order
export function DisplayOrder(order: OrderDetail) {
    return (
        <div className="row payment_orderDetail">
            <div className="l-2 m-2 c-2 payment_orderDetail-img">
                <img src={order.product.photos[0].url} alt="" />
            </div>
            <div className="l-8 l-8 c-8 payment_orderDetail-productInfo">
                <div className='payment_orderDetail-productInfo-name'>
                    <p>{order.product.productName}</p>
                </div>
                <div className='payment_orderDetail-productInfo-color'>
                    <span>Color: </span>
                    <span>{order.product.color.name}</span>
                </div>
                <div className='payment_orderDetail-productInfo-quantity'>
                    <span>Quantity: </span>
                    <span>{order.quantity}</span>
                </div>
            </div>
            <div className="l-2 m-2 c-2 payment_orderDetail-productInfo-price">
                <span>$</span><span>{Number(order.sellPrice) * Number(order.quantity)} </span>
            </div>
        </div>
    )
}

function Payment() {
    const navigate = useNavigate();

    const [orders, setOrders] = useState<OrderDetail[]>
        ([]);
    const [customer, setCustomer] = useState<Customer>(null);
    const userId = Number(localStorage.getItem('userId'));
    const [receiverName, setReceiverName] = useState('');
    const [receiverAddress, setReceiverAddress] = useState('');
    const [receiverPhone, setReceiverPhone] = useState('');

    const [receiverProvince, setReceiverProvince] = useState<string>('');
    const [receiverDistrict, setReceiverDistrict] = useState('');
    const [receiverWard, setReceiverWard] = useState('');
    const [receiverStreet, setReceiverStreet] = useState('');

    const [messageDistrict, setMessageDistrict] = useState('');
    const [messageProvince, setMessageProvince] = useState('');
    const [messageWard, setMessageWard] = useState('');
    const [messageStreet, setMessageStreet] = useState('');

    const [note, setNote] = useState('');
    const [paymentMethod, setPaymentMethod] = useState('COD');
    const [messageReceiverName, setMessageReceiverName] = useState('');
    const [messageReceiverPhone, setMessageReceiverPhone] = useState('');

    const [discount, setDiscount] = useState('');
    const [percent, setPercent] = useState(0);
    const [totalPrice, setTotalPrice] = useState(0);

    const orderData = localStorage.getItem('orders');


    // Gọi API lấy dữ liệu khách hàng
    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/customer/${userId}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(
                        `This is an HTTP error: The status is ${response.status}`
                    );
                }
                return response.json();
            })
            .then((customerData) => {
                setCustomer(customerData);
                setReceiverPhone(customerData.phone);
                setReceiverName(customerData.fullname ? customerData.fullname : "");
                setReceiverProvince(customerData.province?.id);
                setReceiverDistrict(customerData.district?.id);
                setReceiverWard(customerData.ward?.id);
                setReceiverStreet(customerData.street?.id);
            })

        function checkOrder() {
            const orders = JSON.parse(orderData || '[]');
            setOrders(orders);
        }
        window.addEventListener('storage', checkOrder);

        checkOrder();
    }, []);

    // Hàm tính tổng 
    function calculateTotalPrice() {
        const result = orders.reduce((previousValue, currentValue) => {
            return previousValue + (currentValue.quantity * currentValue.sellPrice);
        }, 0)
        setTotalPrice(result);
        return result;
    }

    //Tính tổng số tiền
    function TotalPrice() {
        return (
            <div >
                <span>$</span>
                <span>{calculateTotalPrice()}</span>
            </div>
        )
    }

    // Hàm gọi API gửi order lên hệ thống
    async function callApiPostOrder() {
        const order: Order = {
            customer: {
                id: userId || 0,
            },
            orderDate: new Date().toISOString(),
            shippedDate: '',
            status: 'unpaid',
            orderDetails: JSON.parse(orderData || '[]') || {
                product: {
                    id: 0,
                    originalPrice: 0,
                    productCode: '',
                    productDescription: '',
                    productName: '',
                    quantityInStock: 0,
                    rate: 0,
                    brand: {
                        id: 0,
                        type: '',
                        name: ''
                    },
                    category: {
                        id: 0,
                        type: '',
                        name: ''
                    },
                    color: {
                        id: 0,
                        type: '',
                        name: ''
                    },
                    sellPrice: 0,
                    statusPrice: 0,
                    photos: []
                },
                quantity: 0,
                sellPrice: 0,
                reviewStatus: false,
            },
            note: note,
            paymentMethod: paymentMethod,
            receiverName: receiverName,
            receiverPhone: receiverPhone,
            province: {
                id: Number(receiverProvince),
            },
            district: {
                id: Number(receiverDistrict),
            },
            ward: {
                id: Number(receiverWard),
            },
            street: {
                id: Number(receiverStreet),
            },
        }

        // Hàm kiểm tra dữ liệu
        function validateData() {
            try {
                const orderSchema = object({
                    receiverName: string().required("Please enter receiver name"),
                    province: object({
                        id: number().required("Please select province").positive().integer()
                    }),
                    district: object({
                        id: number().required("Please select district").positive().integer()
                    }),
                    ward: object({
                        id: number().required("Please select ward").positive().integer()
                    }),
                    street: object({
                        id: number().required("Please select street").positive().integer()
                    }),
                    receiverPhone: string().required("Please enter receiver phone"),
                });
                setMessageReceiverName('');
                setMessageReceiverPhone('');
                setMessageDistrict('');
                setMessageProvince('');
                setMessageWard('');
                setMessageStreet('');
                orderSchema.validateSync(order,
                    { abortEarly: false });
                return true;
            } catch (error) {
                if (error instanceof ValidationError) {
                    error.inner.forEach(error => {
                        if (error.path === "receiverName") {
                            setMessageReceiverName(error.message);
                        } else if (error.path === "receiverPhone") {
                            setMessageReceiverPhone(error.message);
                        } else if (error.path === "district.id") {
                            setMessageDistrict('Please select district');
                        } else if (error.path === "province.id") {
                            setMessageProvince('Please select province');
                        } else if (error.path === "ward.id") {
                            setMessageWard('Please select ward');
                        } else if (error.path === "street.id") {
                            setMessageStreet('Please select street');
                        }
                    });
                }

                return false;
            }
        }
        if (validateData()) {
            try {
                const response = await fetch(`${process.env.REACT_APP_API_URL}/order/`, {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        "Content-Type": "application/json",
                        "Authorization": `Bearer ${localStorage.getItem('token')}`,
                    },
                    body: JSON.stringify(order),
                });
                const result = await response.json();
                navigate(`/order-status?id=${result.id}&discount=${percent}&totalPrice=${totalPrice}`)
                localStorage.removeItem("orders");
                window.dispatchEvent(new Event("storage"));

            } catch (error) {
                const errorString = JSON.stringify(error);
                showToastErrorMessage(errorString);
            }
        } else {
            showToastWarningMessage('Please check info again')

        }
    }

    // Hàm gọi API kiểm tra mã discount
    async function callApiCheckDiscount() {
        try {
            const response = await fetch(`${process.env.REACT_APP_API_URL}/discount/find?keyword=${discount.trim()}`, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                },
            });
            const result = await response.json();
            setPercent(result.percent);
        } catch (error) {
            setPercent(0)
            showToastErrorMessage('Discount code is not exist. Please try again');
        }
    }

    return (
        <>
            <div className="checkout__nav">
                <div className="row checkout__nav-wrap">
                    <Link to="/cart" className="checkout_nav-cart">
                        <b>Cart</b>
                    </Link>
                    <div className="detail-nav-icon">
                        <MdKeyboardDoubleArrowRight />
                    </div>
                    <div className=" cart_nav-all-product">
                        <b>Shipping info</b>
                    </div>
                </div>
            </div>
            <div className='checkout__container'>

                <div className='payment_form-container'>
                    <div className="grid">
                        <div className="row payment_form-wrap">
                            <div className="l-6 m-6 c-12 payment_form-leftSide">
                                <h3>Shipping Info</h3>
                                <form className='payment_form' action="">
                                    <div className='payment_form-item'>
                                        <label htmlFor={`payment_fullname`}>
                                            <b>Receiver name <span style={{ color: "red" }}>*</span></b>
                                        </label>
                                        <Input id={`payment_fullname`} message={messageReceiverName} className={`register__fullname`} type="text" placeholder='Fullname' value={receiverName} onChange={(event) => setReceiverName(event.target.value)} />
                                    </div>
                                    <div className='payment_form-item'>
                                        <label htmlFor="payment_phone">
                                            <b>Receiver Phone
                                                <span style={{ color: "red" }}> *</span>
                                            </b>
                                        </label>
                                        <Input id={`payment_phone`} message={messageReceiverPhone} className={`register__phone`} type="text" placeholder='Phone' value={receiverPhone} onChange={(event) => setReceiverPhone(event.target.value)} />
                                    </div>
                                    <div>
                                        <p style={{ marginBottom: "10px" }}>
                                            <b>Receiver Address
                                                <span style={{ color: "red" }}>*</span>
                                            </b>
                                        </p>
                                        <Locations
                                            receiverProvince={receiverProvince}
                                            setReceiverProvince={setReceiverProvince}

                                            receiverDistrict={receiverDistrict}
                                            setReceiverDistrict={setReceiverDistrict}

                                            receiverWard={receiverWard}
                                            setReceiverWard={setReceiverWard}

                                            receiverStreet={receiverStreet}
                                            setReceiverStreet={setReceiverStreet}

                                            messageProvince={messageProvince}
                                            messageDistrict={messageDistrict}
                                            messageWard={messageWard}
                                            messageStreet={messageStreet}
                                        />
                                    </div>
                                    <div className='payment_form-item'>
                                        <label htmlFor="input-note">
                                            <b>Note</b>
                                        </label>
                                        <input onChange={e => setNote(e.target.value)} id="input-note" value={note} type="text" />
                                    </div>
                                    <div className='payment_form-item-checkbox'>
                                        <p>
                                            <b>Payment methods
                                                <span style={{ color: "red" }}>*</span>
                                            </b>
                                        </p>
                                        <div className='payment_form-item-checkbox-detail'>
                                            <input checked={paymentMethod === "COD"} onChange={e => {
                                                if (e.target.checked) {
                                                    setPaymentMethod('COD')
                                                }
                                            }} name="paymentMethod" id="COD" type="radio" />
                                            <label htmlFor="COD">COD</label>
                                        </div>
                                        <div className='payment_form-item-checkbox-detail'>
                                            <input checked={paymentMethod === "card"} onChange={e => {
                                                if (e.target.checked) {
                                                    setPaymentMethod('card')
                                                }
                                            }} name="paymentMethod" id="card" type="radio" />
                                            <label htmlFor="card">Card</label>
                                        </div>
                                    </div>
                                    <div className='payment_form-item-btn mobile-hidden'>
                                        <div className="row">
                                            <div className="l-6 m-6 c-6 payment_form-item-backToCart">
                                                <Link to="/cart">
                                                    <span>Back to cart</span>
                                                </Link>
                                            </div>
                                            <div className="l-6 m-6 c-6 payment_form-item-button">
                                                <button type="button" onClick={callApiPostOrder}>Complete Order</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="l-6 m-6 c-12 payment_form-rightSide">
                                <div className="payment_form-rightSide-wrap">
                                    <div className="payment_orderDetail-wrap">
                                        {orders.map((orderDetail) => {
                                            return (
                                                <DisplayOrder key={orderDetail.product.id} {...orderDetail} />
                                            );
                                        })}
                                    </div>
                                    <div className="payment_discount">
                                        <div className="payment_discount-input">
                                            <input id="payment_discount-number" value={discount} type="text" onChange={e => setDiscount(e.target.value)} placeholder='Enter discount code' />
                                            <button disabled={!discount.trim()} onClick={callApiCheckDiscount}>Apply</button>
                                            <ToastContainer />
                                        </div>
                                    </div>
                                    <div className="payment_calculation">
                                        <div className="payment_calculation-productPrice">
                                            <div>
                                                Price
                                            </div>
                                            <div>
                                                <TotalPrice />
                                            </div>
                                        </div>
                                        <div className="payment_calculation-discount">
                                            <div>Discount</div>
                                            <div>
                                                <span>$</span>
                                                <span id='payment_calculation-discount-number'>{(percent * totalPrice) / 100}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='payment_totalPrice'>
                                        <div>Total price</div>
                                        <div className='payment_totalNumber'>
                                            <b>$</b>
                                            <span style={{ fontWeight: "700" }}>{totalPrice - ((percent * totalPrice) / 100)}</span>
                                        </div>
                                    </div>
                                    <p className='payment_totalPrice-message'>Shipping fee not included</p>
                                </div>
                            </div>
                            <div className='payment_form-item-btn mobile-show'>
                                <div className="row">
                                    <div className="l-6 m-6 c-12 payment_form-item-button">
                                        <button type="button" onClick={callApiPostOrder}>
                                            Complete Order
                                        </button>
                                    </div>
                                    <div className="l-6 m-6 c-12 payment_form-item-backToCart">
                                        <Link to="/cart">
                                            <span>Back to cart</span>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div >
            </div >
        </>
    )
}

export default Payment;