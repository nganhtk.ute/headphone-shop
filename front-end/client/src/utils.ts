import { OrderDetail } from "./Products";

function calculateTotalPrice(orderDetails: OrderDetail[]) {
    const result = orderDetails.reduce(function (previousValue, currentValue) {
        return previousValue + (currentValue.quantity + currentValue.sellPrice)
    }, 0)

    return result;
}







