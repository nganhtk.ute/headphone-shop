import React, { ChangeEventHandler, useState } from 'react';
import { MdKeyboardDoubleArrowRight } from "react-icons/md";
import './Products.css';
import './Grid.css';
import './Login.css';
import './Cart.css';
import { ValidationError, number, object, string } from 'yup';
import { Link, useNavigate } from 'react-router-dom';
import { showToastErrorMessage, showToastWarningMessage } from './ChangePassword';
import { ToastContainer } from 'react-toastify';

interface InputProps {
  message: string;
  id: string;
  className: string;
  placeholder: string;
  type: string;
  onChange: ChangeEventHandler<HTMLInputElement>;
  value: string;
}

function Input(props: InputProps) {
  const { message, id, className, placeholder, type, value, onChange } = props;

  return (
    <div>
      <input id={id} className={`${className} ${message ? 'input-invalid' : ''}`} type={type} placeholder={placeholder} value={value} onChange={onChange} />
      <div className={`invalid-message ${message ? '' : 'hidden'}`}>{message}</div>
    </div>
  )
}


function Login() {
  const navigate = useNavigate();

  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const [messagePassword, setMessagePassword] = useState('');
  const [messageUsername, setMessageUsername] = useState('');

  async function callApiLogin() {
    const data = {
      username,
      password
    };
    function validateData() {
      try {
        const userSchema = object({
          username: string().required("Please enter username"),
          password: string().required("Please enter password"),
        });
        setMessagePassword('');
        setMessageUsername('');
        userSchema.validateSync(data,
          { abortEarly: false });

        return true;
      } catch (error) {
        if (error instanceof ValidationError) {
          error.inner.forEach(error => {
            if (error.path === "username") {
              setMessageUsername(error.message);
            } else if (error.path === "password") {
              setMessagePassword(error.message);
            }
          });
        }

        return false;
      }
    }
    if (validateData()) {
      const token = localStorage.getItem('token');

      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/login-client`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            // "Authorization": `Bearer ${token}`,
          },
          body: JSON.stringify(data),
        });
        const result = await response.json();

        localStorage.setItem('token', result.token);
        localStorage.setItem('username', result.username);
        localStorage.setItem('userId', result.id);

        navigate('/')
      } catch (error) {
        showToastErrorMessage("Username or Password is not accurate. Please try again");
      }
    } else {
      showToastWarningMessage('Please check info again')
    }
  }


  return (
    <>
      <div className="login__nav">
        <div className="row login__nav-wrap">
          <Link to="/" className="nav-home">
            <b>Home</b>
          </Link>
          <div className=" detail-nav-icon"><MdKeyboardDoubleArrowRight /></div>
          <div className="nav-all-product">
            <b>Login</b>
          </div>
        </div>
      </div>
      <div className='login__container'>
        <div className="row">
          <div className="l-4 m-2 c-2"></div>
          <div className="l-4 m-8 c-8">
            <form className="login__form">
              <div className="login__title">
                <h1>Login</h1>
                <p>If you do not have an account yet. Please register&nbsp;
                  <Link to="/register">here</Link>
                </p>
              </div>
              <Input message={messageUsername} id="login__username" className='login__username' type="text" placeholder='Username' value={username} onChange={(event) => setUsername(event.target.value)} />
              <Input message={messagePassword} id="login__password" className='login__password' type="password" placeholder='Password' value={password} onChange={(event) => setPassword(event.target.value)} />
              <button type='button' onClick={callApiLogin} className='login_btn'>Login</button>
              <ToastContainer />
              {/* <a className='login__forgotPassword' href="">Forgot Password?</a> */}
            </form>
          </div>
          <div className="l-4 m-2 c-2"></div>
        </div>
      </div>
    </>
  );
}

export default Login;

