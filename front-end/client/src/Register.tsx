import React, { ChangeEventHandler, useEffect, useState } from 'react';
import { MdKeyboardDoubleArrowRight, MdFilterAlt, MdClear } from "react-icons/md";
import './Products.css';
import './Grid.css';
import './Register.css';
import './Login.css';
import './Cart.css';
import { Customer, showToastSuccessMessage } from './ProductItem';
import { ValidationError, number, object, string } from 'yup';
import { Link, useNavigate } from 'react-router-dom';
import { showToastErrorMessage, showToastWarningMessage } from './ChangePassword';
import { ToastContainer } from 'react-toastify';


interface InputProps {
  message: string;
  id: string;
  className: string;
  placeholder: string;
  type: string;
  onChange: ChangeEventHandler<HTMLInputElement>;
  value: string;
}


export function Input(props: InputProps) {
  const { message, id, className, placeholder, type, value, onChange } = props;

  return (
    <div>
      <input id={id} className={`${className} ${message ? 'input-invalid' : ''}`} type={type} placeholder={placeholder} value={value} onChange={onChange} />
      <div className={`invalid-message ${message ? '' : 'hidden'}`}>{message}</div>
    </div>
  )
}

function Register() {
  const navigate = useNavigate();

  const [username, setUsername] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [password, setPassword] = useState<string>('');

  const [messagePassword, setMessagePassword] = useState('');
  const [messagePhone, setMessagePhone] = useState('');
  const [messageUsername, setMessageUsername] = useState('');

  async function callApiRegister() {
    const data = {
      username,
      password,
      phone,
    };
    function validateData() {
      try {
        const userSchema = object({
          username: string().required("Please enter username"),
          phone: string().length(10, "Phone number must be 10 digits").required("Please enter phone"),
          password: string().required("Please enter password"),
        });
        setMessagePhone('');
        setMessagePassword('');
        setMessageUsername('');
        userSchema.validateSync(data,
          { abortEarly: false });

        return true;
      } catch (error) {
        if (error instanceof ValidationError) {
          error.inner.forEach(error => {
            if (error.path === "phone") {
              setMessagePhone(error.message);
            } else if (error.path === "username") {
              setMessageUsername(error.message);
            } else if (error.path === "password") {
              setMessagePassword(error.message);
            }
          });
        }

        return false;
      }
    }


    if (validateData()) {
      try {
        const response = await fetch(`${process.env.REACT_APP_API_URL}/register`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data),
        });

        if (response.ok) {
          showToastSuccessMessage("You register successful")

        } else {
          throw await response.text();
        }
      } catch (error) {
        const errorString = JSON.stringify(error);
        showToastErrorMessage(errorString);
      }
    } else {
      showToastWarningMessage('Please check info again')

    }
  }

  return (
    <>
      <div className="login__nav">
        <div className="row login__nav-wrap">
          <Link to="/" className="nav-home">
            <b>Home</b>
          </Link>
          <div className="detail-nav-icon">
            <MdKeyboardDoubleArrowRight />
          </div>
          <div className="nav-register">
            <b>Register</b>
          </div>
        </div>
      </div>

      <div className='login__container'>
        <div className="row">
          <div className="l-4 m-2 c-2"></div>
          <div className="l-4 m-8 c-8">
            <form className="register__form">
              <div className="register__title">
                <h1>Register</h1>
                <p>Already have an account? Please login&nbsp;
                  <Link to="/login">
                    here
                  </Link>
                </p>
              </div>
              <Input message={messagePhone} id="register__phone-input" className={`register__phone`} type="text" placeholder='Phone' value={phone} onChange={(event) => setPhone(event.target.value)} />
              <Input message={messageUsername} id="register__username-input" className='register__username' type="text" placeholder='Username' value={username} onChange={(event) => setUsername(event.target.value)} />
              <Input message={messagePassword} id="register__password-input" className='register__password' type="password" placeholder='Password' value={password} onChange={(event) => setPassword(event.target.value)} />
              <button type='button' onClick={callApiRegister} className='register_btn'>Register
              </button>
              <ToastContainer />
            </form>
          </div>
          <div className="l-4 m-2 c-2"></div>
        </div>
      </div>
    </>
  );
}

export default Register;

