import React, { useState } from 'react';
import { CiCircleChevRight, CiCircleChevLeft } from "react-icons/ci";
import './Grid.css';
import './Slider.css';
import { useNavigate } from 'react-router-dom';

export interface File {
    id: number;
    name: string;
    originalName: string;
    url: string;
}

export interface ProductLine {
    id: number;
    type: string;
    name: string;
}

export interface Product {
    id: number;
    originalPrice: number;
    productCode: string;
    productDescription: string;
    productName: string;
    quantityInStock: number;
    rate: number;
    brand: ProductLine;
    category: ProductLine
    color: ProductLine;
    sellPrice: number;
    statusPrice: number;
    photos: File[];
    status: string;
    productGuarantee: string;
}

interface SliderProps {
    productList: Product[]
}

function Slider(props: SliderProps) {
    const navigate = useNavigate();

    const productList = props.productList;
    const [currentPictureIndex, setCurrentPictureIndex] = useState(0);
    const currentProduct = productList[currentPictureIndex];

    function previousClick() {
        if (currentPictureIndex === 0) {
            setCurrentPictureIndex(productList.length - 1);
        } else {
            setCurrentPictureIndex(currentPictureIndex - 1);
        }
    }

    function nextClick() {
        if (currentPictureIndex === (productList.length - 1)) {
            setCurrentPictureIndex(0);
        } else {
            setCurrentPictureIndex(currentPictureIndex + 1);
        }
    }

    if (productList.length === 0) {
        return null;
    }

    function GoToDetailPage() {
        navigate(`/Detail?id=${currentProduct.id}`)
    }

    return (
        <div className='slider__container'>
            <div className="grid wide">
                <div className="row slider__content">
                    <div className='l-1 m-1 c-1 slide_prev-btn' onClick={previousClick}>
                        <CiCircleChevLeft style={{ cursor: "pointer" }} />
                    </div>
                    <div className='l-10 m-10 c-10'>
                        <div className="slider__info">
                            <div className="row slider__info-align-center">
                                <div className="l-1 m-1 c-1"></div>
                                <div className='l-5 m-5 c-5 slider-left'>
                                    <div className='slider-brand'>
                                        <p>{currentProduct.brand?.name}</p>
                                    </div>
                                    <div className='slider-name'>
                                        <h1>{currentProduct.productName}</h1>
                                    </div>
                                    <div className='slider-description'>
                                        {currentProduct.productDescription}
                                    </div>
                                    <div className='slider-button'>
                                        <button data-id={currentProduct.id} onClick={GoToDetailPage}>
                                            SHOP NOW
                                        </button>
                                    </div>
                                </div>
                                <div className='l-5 m-5 c-5 slider-right'>
                                    <img src={currentProduct.photos[0]?.url} alt="" />
                                </div>
                                <div className="l-1 m-1 c-1"></div>
                            </div>
                        </div>
                    </div>
                    <div className='l-1 m-1 c-1 slider_next-btn' onClick={nextClick}>
                        <CiCircleChevRight style={{ cursor: "pointer" }} />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Slider;