import React, { useEffect, useState } from 'react';
import { MdKeyboardDoubleArrowRight, MdShoppingCart } from "react-icons/md";
import { FaStar } from 'react-icons/fa';
import { FaRegStar } from "react-icons/fa6";
import './Detail.css';
import './Grid.css';
import './ProductItem.css';
import { Product } from './Slider';
import ProductItem from './ProductItem';
import Review, { File } from './Review';
import { OrderDetail } from './Products';
import { Link, redirect, useNavigate } from 'react-router-dom';

export interface Review {
  id: number;
  product: number;
  customer: string;
  reviewText: string;
  rate: number;
  createDate: Date;
  photos: File[]

}
export interface RatingProps {
  value: number;
}

export interface ReviewProps {
  url: string;
}

export interface Stock {
  value: number;
}

export interface CreateDate {
  createDate: Date;
}

export function Rating(props: RatingProps) {
  const STAR_COUNT = 5;
  const { value } = props;

  const stars = Array.from({ length: STAR_COUNT }, (_, i) => {
    if (i < value) {
      return (<FaStar key={i} />)
    }

    return (<FaRegStar key={i} />)
  });

  return <div className="rating">{stars}</div>;
}

export function ReviewImage(props: ReviewProps) {
  const { url } = props;
  return <><img src={url} alt="" /></>
}

export function DisplayCreateDate(props: CreateDate) {
  const { createDate } = props;
  const formatDate = new Date(createDate).toLocaleDateString('en-GB', { timeZone: 'UTC' })

  return (
    <div className='detail-review-item_createDate'>
      {formatDate}
    </div>
  )
}

export function DisplayReview(review: Review) {

  return (
    <div className="detail-review-item">
      <div className='detail-review-item_user'>
        <div>{review.customer}</div>
        <Rating value={review.rate} />
        <DisplayCreateDate createDate={review.createDate} />
      </div>
      <div className='detail-review-item_text'>
        {review.reviewText}
      </div>
      <div className='detail-review-item_img'>
        {review.photos?.map((item) => (
          <ReviewImage key={item.id} url={item.url} />
        ))
        }
      </div>
    </div>
  )
}

function Detail() {
  const navigate = useNavigate();

  const [productList, setProductList] = useState<Product[]>([]);
  const [reviewList, setReviewList] = useState<Review[]>([]);
  const [productItem, setProductItem] = useState<Product>();
  const [pageNum, setPageNum] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [disabled, setDisabled] = useState(false)
  const [disableStyle, setDisabledStyle] = useState('');
  const [averageRate, setAverageRate] = useState(0);
  const [allRate, setAllRate] = useState(0);
  const [fiveRate, setFiveRate] = useState(0);
  const [twoRate, setTwoRate] = useState(0);
  const [threeRate, setThreeRate] = useState(0);
  const [fourRate, setFourRate] = useState(0);
  const [oneRate, setOneRate] = useState(0);
  const [comment, setComment] = useState(0);
  const [rate, setRate] = useState('');
  const [keyword, setKeyword] = useState('');
  const [product, setProduct] = useState(0);
  const [styleAllBtn, setStyleAllBtn] = useState('btnAll-active');
  const [styleFiveBtn, setStyleFiveBtn] = useState('');
  const [styleFourBtn, setStyleFourBtn] = useState('');
  const [styleThreeBtn, setStyleThreeBtn] = useState('');
  const [styleTwoBtn, setStyleTwoBtn] = useState('');
  const [styleOneBtn, setStyleOneBtn] = useState('');
  const [styleCommentBtn, setStyleCommentBtn] = useState('');
  const [btnPageOne, setBtnPageOne] = useState('pagination_active');
  const [btnPageTwo, setBtnPageTwo] = useState('');
  const [btnPageThree, setBtnPageThree] = useState('');
  const [btnPageFour, setBtnPageFour] = useState('');
  const [btnPageFive, setBtnPageFive] = useState('');
  const [mainImage, setMainImage] = useState<string>('');
  const [quantity, setQuantity] = useState(1);

  const searchParams = new URLSearchParams(window.location.search);
  const idProduct = Number(searchParams.get('id'));

  // Hàm xử lý sự kiện hiển thị main image
  function DisplayMiniImage(props: File) {
    const { id, name, originalName, url } = props;
    return (
      <div className="l-3 m-3 c-3 detail-picture-item">
        <img onClick={() => setMainImage(url)} src={url} alt="" />
      </div>
    )
  }

  // Gọi API lấy thông tin sản phẩm theo productID
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/product/${idProduct}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setMainImage(actualData.photos[0].url);
        setProduct(actualData.id);
        setProductItem(actualData);
      })
      .catch((err) => {
        setProductItem(undefined);
      })
  }, []);

  // Hàm xử lý hiển thị sold out
  function StatusMessage(props: Stock) {
    const { value } = props;
    if (value > 0) {
      return (
        <div id="detail-rated-star-wrap" className='detail-rated-star'>
          Available
        </div>
      )
    } else {
      return (
        <div id="detail-rated-star-wrap" className='detail-rated-star'>
          Sold out
        </div>
      )
    }
  }

  // Gọi API lấy tất cả review theo productID
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/review/review-list?product=${idProduct}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setOneRate(0);
        setTwoRate(0);
        setThreeRate(0);
        setFourRate(0);
        setFiveRate(0);
        setComment(0);
        const initialValue = 0;
        const sumWithInitial = actualData.reduce(
          (accumulator: number, currentValue: Review) => accumulator + currentValue.rate,
          initialValue,
        );
        setAverageRate(Number((sumWithInitial / actualData.length).toFixed(1)));
        setAllRate(actualData.length)

        let countOneRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 1) {
            countOneRate += 1
          }
        }
        setOneRate(countOneRate)


        let countTwoRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 2) {
            countTwoRate += 1;
          }
        }
        setTwoRate(countTwoRate);

        let countThreeRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 3) {
            countThreeRate += 1;
          }
        }
        setThreeRate(countThreeRate);

        let countFourRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 4) {
            countFourRate += 1;
          }
        }
        setFourRate(countFourRate);

        let countFiveRate = 0;
        for (let index = 0; index < actualData.length; index++) {
          if (actualData[index].rate === 5) {
            countFiveRate += 1;
          }
        }
        setFiveRate(countFiveRate);
      })
      .catch((err) => {
        setProductItem(undefined);
      })
  }, []);

  // Gọi API lấy tất cả sản phẩm
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/product/paging`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setProductList(actualData.content);
      })
      .catch((err) => {
        setProductItem(undefined);
      })
      .finally(() => {
      })
  }, []);

  // Gọi API lấy review có comment
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/review/review-list-text?product=${idProduct}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setComment(actualData.totalElements);
      })
      .catch((err) => {
        setProductItem(undefined);
      })
      .finally(() => {
      })
  }, []);

  //Gọi API lấy review theo filter
  useEffect(() => {
    const searchParams = new URLSearchParams();

    searchParams.append('page', String(pageNum));
    searchParams.append('size', String(2));

    if (rate !== '') {
      searchParams.append('rate', String(rate));
    }

    searchParams.append('keyword', String(keyword));

    if (product !== 0) {
      searchParams.append('product', String(product));
    }

    fetch(`${process.env.REACT_APP_API_URL}/review/paging1?${searchParams}`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `This is an HTTP error: The status is ${response.status}`
          );
        }
        return response.json();
      })
      .then((actualData) => {
        setReviewList(actualData.content)
        setTotalPages(actualData.totalPages);
      })
  }, [pageNum, product, rate, keyword]);

  const [style, setStyle] = useState("half-height");

  // Hàm thay đổi kích thước description picture
  const changeStyle = () => {
    if (style !== "half-height") {
      setStyle("half-height");

    } else { setStyle(""); }
  };

  // Hàm xử lý nút previous
  function PreviousButton() {
    // if (pageNum === 0) {
    //   setBtnPageOne('pagination_active');
    //   setBtnPageTwo('');
    //   setBtnPageThree('');
    //   setBtnPageFour('');
    //   setBtnPageFive('');
    // } else if (pageNum === 1) {
    //   setBtnPageOne('');
    //   setBtnPageTwo('pagination_active');
    //   setBtnPageThree('');
    //   setBtnPageFour('');
    //   setBtnPageFive('');
    // } else if (pageNum === 2) {
    //   setBtnPageOne('');
    //   setBtnPageTwo('');
    //   setBtnPageThree('pagination_active');
    //   setBtnPageFour('');
    //   setBtnPageFive('');
    // } else if (pageNum === 3) {
    //   setBtnPageOne('');
    //   setBtnPageTwo('');
    //   setBtnPageThree('');
    //   setBtnPageFour('pagination_active');
    //   setBtnPageFive('');
    // } else if (pageNum === 4) {
    //   setBtnPageOne('');
    //   setBtnPageTwo('');
    //   setBtnPageThree('');
    //   setBtnPageFour('');
    //   setBtnPageFive('pagination_active');
    // }
    return (
      <button disabled={pageNum === 0} onClick={() => setPageNum(pageNum - 1)}>&laquo;</button>
    )
  }

  // Hàm xử lý nút next
  function NextButton() {
    // if (pageNum === 0) {
    //   setBtnPageOne('pagination_active');
    //   setBtnPageTwo('');
    //   setBtnPageThree('');
    //   setBtnPageFour('');
    //   setBtnPageFive('');
    // } else if (pageNum === 1) {
    //   setBtnPageOne('');
    //   setBtnPageTwo('pagination_active');
    //   setBtnPageThree('');
    //   setBtnPageFour('');
    //   setBtnPageFive('');
    // } else if (pageNum === 2) {
    //   setBtnPageOne('');
    //   setBtnPageTwo('');
    //   setBtnPageThree('pagination_active');
    //   setBtnPageFour('');
    //   setBtnPageFive('');
    // } else if (pageNum === 3) {
    //   setBtnPageOne('');
    //   setBtnPageTwo('');
    //   setBtnPageThree('');
    //   setBtnPageFour('pagination_active');
    //   setBtnPageFive('');
    // } else if (pageNum === 4) {
    //   setBtnPageOne('');
    //   setBtnPageTwo('');
    //   setBtnPageThree('');
    //   setBtnPageFour('');
    //   setBtnPageFive('pagination_active');
    // }
    return (
      <button disabled={pageNum === 4} onClick={() => setPageNum(pageNum + 1)}>&raquo;</button>
    )
  }

  //Hàm lưu orderDetail vào localStorage
  function saveLocalStorage() {
    const token = localStorage.getItem('token');

    const orderList: OrderDetail[] = JSON.parse(localStorage.getItem('orders') || '[]');

    if (token) {
      const index = orderList.findIndex(orderDetail =>
        orderDetail.product.id === productItem?.id
      );

      if (index === -1 && productItem) {
        const orderDetail: OrderDetail = {
          product: productItem,
          quantity: quantity,
          sellPrice: productItem?.sellPrice,
          reviewStatus: false,
        }

        orderList.push(orderDetail);
      } else {
        orderList[index].quantity += 1;
      }

      localStorage.setItem("orders", JSON.stringify(orderList));
      window.dispatchEvent(new Event("storage"));
    } else {
      navigate("/login")
    }
  }

  return (
    <>
      <div className="detail__nav">
        <div className="row detail__nav-wrap">
          <Link className="detail_nav-home" to="/">
            <b>Home</b>
          </Link>
          <div className="detail-nav-icon">
            <MdKeyboardDoubleArrowRight />
          </div>
          <a href="/products" className="detail_nav-all-product">
            <b>All Products</b>
          </a>
          <div className="detail_detail-nav-icon">
            <MdKeyboardDoubleArrowRight />
          </div>
          <div className="detail_nav-all-productName">
            <b>{productItem?.productName}</b>
          </div>
        </div>
      </div>

      <div className='detail__container'>
        <div className="grid wide">
          <div className="row detail__content">
            <div className='l-12 m-12 c-12'>
              <div className="detail__info">
                <div className="row">
                  <div className='l-4 m-4 c-5 detail__left'>
                    <div className="detail-main-picture">
                      <img src={mainImage} alt="" />
                    </div>
                    <div className="detail-pictureList-container">
                      <div className="row detail-row-pictureList">
                        <div className="l-8 m-8 c-8">
                          <div className="row detail-pictureList">
                            {productItem?.photos.map((item, i) => (<DisplayMiniImage key={i} {...item} />))}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="l-1 m-1 c-0"></div>
                  <div className='l-7 m-7 c-7 detail-right'>
                    <div className='detail-name'>
                      <h1>{productItem?.productName}</h1>
                    </div>
                    <div className='detail-brand'>
                      <span>Brand:</span> <span className='detail-brand-info'>{productItem?.brand.name}</span>
                    </div>
                    <div className='detail-rated'>
                      <div className='detail-rated-text'>
                        <p>Status:</p>
                      </div>
                      <StatusMessage value={Number(productItem?.quantityInStock)} />
                    </div>
                    <div className='detail-guarantee'>
                      <span>Guarantee:</span>
                      <span className='detail-guarantee-text'>{productItem?.productGuarantee}</span>
                    </div>
                    <div className='detail-price'>
                      <b>${productItem?.sellPrice}</b>
                    </div>
                    <div className='detail-adjust-quantity'>
                      <button onClick={() => { quantity >= 2 ? setQuantity(quantity - 1) : setQuantity(1) }} className='detail-minus'>
                        -
                      </button>
                      <span className="detail-quantity">{quantity}</span>
                      <button onClick={() => setQuantity(quantity + 1)} className='detail-plus'>
                        +
                      </button>
                    </div>
                    <div className='detail-button'>
                      <button onClick={() => saveLocalStorage()} disabled={productItem?.quantityInStock === 0} className={`detail-button_btn ${productItem?.quantityInStock === 0 ? 'disabledButton' : ''}`}>
                        ADD TO CART
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={`detail-description-container `}>
        <div className={`grid wide ${style}`}>
          <div className="row">
            <div className="l-12 m-12 c-12 detail-description-title">
              <h1 style={{ fontSize: "1rem" }}>
                Description
              </h1>
              <div className='detail-description'>
                <p>
                  {productItem?.productDescription}
                </p>
                <div className={`detail-description-image`}>
                  <img className="full-size-image" src={productItem?.photos[0]?.url} alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div >
      <div className='l-12 m-12 c-12 recss-detail-btn detail-button'>
        <button id="detail-btn-view" onClick={changeStyle}>{style === 'half-height' ? 'VIEW ALL' : 'VIEW LESS'}</button>
      </div>
      <div className='detail-review-container'>
        <div className='detail-review_wrap'>
          <div className='detail-review-header'>
            <h2>REVIEW</h2>
          </div>
          <div className="detail-review-body">
            <div className="detail-review-filter">
              <div className='detail-review-filter_wrap row'>
                <div className="m-3">
                  <div>
                    <span className='detail-review-filter_totalRate'>{averageRate}</span><span className='detail-review-filter_maxRate'> / 5</span>
                  </div>
                  <Rating value={Math.round(averageRate)} />
                </div>
                <div className="m-6 detail-review_filter-btn">
                  <button onClick={e => {
                    setRate('');
                    setStyleAllBtn('btnAll-active');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setKeyword('');
                  }} className={`detail-review_filter-btn-all ${styleAllBtn}`}>All <span>({allRate})</span>
                  </button>
                  <button onClick={e => {
                    setRate('5');
                    setStyleAllBtn('');
                    setStyleFiveBtn('btnFive-active');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setKeyword('');
                  }} className={`${styleFiveBtn}`}>5 stars <span>({fiveRate})</span></button>
                  <button onClick={e => {
                    setRate('4');
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('btnFour-active');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setKeyword('');
                  }} className={`${styleFourBtn}`}>4 stars <span>({fourRate})</span></button>
                  <button onClick={e => {
                    setRate('3');
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('btnThree-active');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setKeyword('');
                  }} className={`${styleThreeBtn}`}>3 stars <span>({threeRate})</span></button>
                  <button onClick={e => {
                    setRate('2');
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('btnTwo-active');
                    setStyleOneBtn('');
                    setStyleCommentBtn('');
                    setKeyword('');

                  }} className={`${styleTwoBtn}`}>2 stars <span>({twoRate})</span></button>
                  <button onClick={e => {
                    setRate('1');
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('btnOne-active');
                    setStyleCommentBtn('');
                    setKeyword('');
                  }} className={`${styleOneBtn}`}>1 star <span>({oneRate})</span></button>
                  <button onClick={e => {
                    setStyleAllBtn('');
                    setStyleFiveBtn('');
                    setStyleFourBtn('');
                    setStyleThreeBtn('');
                    setStyleTwoBtn('');
                    setStyleOneBtn('');
                    setStyleCommentBtn('btnComment-active');
                    setKeyword('.');
                  }} className={`${styleCommentBtn}`}>Have comment <span>({comment})</span></button>
                </div>
              </div>
              <div className='detail-review-list'>
                {reviewList.map((item, i) => (
                  <DisplayReview key={i} {...item} />
                ))}
              </div>
            </div>
          </div>
          <div className="pagination">
            <PreviousButton />
            {/* <button className={`${btnPageOne}`} onClick={() => { setPageNum(0); setBtnPageOne('pagination_active'); setBtnPageTwo(''); setBtnPageThree(''); setBtnPageFour(''); setBtnPageFive('') }}>
              <a href="">1</a>
            </button>
            <button className={`${btnPageTwo}`} onClick={() => { setPageNum(1); setBtnPageOne(''); setBtnPageTwo('pagination_active'); setBtnPageThree(''); setBtnPageFour(''); setBtnPageFive('') }} >
              <a href="">2</a>
            </button>
            <button className={`${btnPageThree}`} onClick={() => { setPageNum(2); setBtnPageOne(''); setBtnPageTwo(''); setBtnPageThree('pagination_active'); setBtnPageFour(''); setBtnPageFive('') }}>
              <a href="">3</a>
            </button>
            <button className={`${btnPageFour}`} onClick={() => { setPageNum(3); setBtnPageOne(''); setBtnPageTwo(''); setBtnPageThree(''); setBtnPageFour('pagination_active'); setBtnPageFive('') }}>
              <a href="">4</a>
            </button>
            <button className={`${btnPageFive}`} onClick={() => { setPageNum(4); setBtnPageOne(''); setBtnPageTwo(''); setBtnPageThree(''); setBtnPageFour(''); setBtnPageFive('pagination_active') }}>
              <a href="">5</a>
            </button> */}
            <NextButton />
          </div>
        </div>
      </div >
      <div className='productList-container'>
        <div className="grid">
          <div className="row">
            <h1 className='l-12 m-12 c-12 detail-title'>Related Products</h1>
            <div className="productItem-container">
              <div className="row" id="productList-wrap">
                {
                  productList.map((item, i) => (<ProductItem key={i} product={item} />))
                }
              </div>
            </div>
          </div>
        </div>
      </div>

    </>
  );
}

export default Detail;

