import React, { useEffect, useState } from 'react';
import { MdKeyboardDoubleArrowRight } from "react-icons/md";
import { GrDocumentText } from "react-icons/gr";
import { FaRegUser } from "react-icons/fa";

import { FaLock } from "react-icons/fa";
import { FiLock } from "react-icons/fi";
import './Grid.css';
import './Payment.css';
import './Profile.css';
import './MyOrder.css';
import { OrderDetail } from './Products';
import { Customer } from './ProductItem';
import { ValidationError, object, string } from 'yup';
import { Input } from './Register';
import { Order } from './Payment';
import { Product } from './Slider';
import { Link, useNavigate } from 'react-router-dom';
// import { CreateDate } from './Detail';

interface DisplayOrderProps {
    orderDetail: OrderDetail;
    orderId: number | undefined;
    orderDate: string;
}
interface CreateDate {
    createDate: string;
}
function MyOrder() {
    const navigate = useNavigate();

    const [username, setUsername] = useState('');
    const [fullname, setFullname] = useState('');
    const [email, setEmail] = useState('');
    const [address, setAddress] = useState('');
    const [phone, setPhone] = useState('');
    const [order, setOrder] = useState<Order[]>();
    const [files, setFiles] = useState([]);
    const searchParams = new URLSearchParams(window.location.search);
    const userId = Number(searchParams.get('id'));
    const [customer, setCustomer] = useState<Customer>();

    const idOrder = Number(searchParams.get('id'));

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/customer/${userId}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(
                        `This is an HTTP error: The status is ${response.status}`
                    );
                }
                return response.json();
            })
            .then((actualData) => {
                setOrder(actualData.orderList);
                setCustomer(actualData);
                setUsername(actualData.username);
                setFullname(actualData.fullname);
                setEmail(actualData.email);
                setPhone(actualData.phone);
                setAddress(actualData.address);
            })
    }, []);

    function DisplayOrder(props: DisplayOrderProps) {
        const { orderDetail, orderId, orderDate } = props;

        function ButtonReview() {
            if (orderDetail.reviewStatus === false) {
                return (
                    <button onClick={() => {
                        return (
                            navigate(`/review?productId=${orderDetail?.product.id}&orderId=${orderId}&orderDetail=${orderDetail.id}`)
                        )
                    }} className={`myOrder_button-review`}>
                        Review
                    </button>
                )
            }
        }
        function DisplayCreateDate(orderDate: CreateDate) {
            const { createDate } = orderDate;
            const formatDate = new Date(createDate).toLocaleDateString('en-GB', { timeZone: 'UTC' })

            return (
                <div style={{ color: "black" }} className="myOrder_status">
                    <span>
                        Order date:   {formatDate}
                    </span>
                </div>
            )
        }
        return (
            <div className="myOrder_container-item">
                <DisplayCreateDate createDate={orderDate} />
                <div className="row myOrder_orderDetail">
                    <div className="l-2 m-2 c-2 myOrder_orderDetail-img">
                        <img src={orderDetail?.product.photos[0].url} alt="" />
                    </div>
                    <div className="l-8 l-8 c-8 myOrder_orderDetail-productInfo">
                        <div className='myOrder_orderDetail-productInfo-name'>
                            <p>{orderDetail?.product.productName}</p>
                        </div>
                        <div className='myOrder_orderDetail-productInfo-color'>
                            <span style={{ color: "grey" }}>Color: </span>
                            <span style={{ color: "grey" }}>{orderDetail?.product.color.name}</span>
                        </div>
                        <div className='myOrder_orderDetail-productInfo-quantity'>
                            <span>Quantity: </span>
                            <span>{orderDetail?.quantity}</span>
                        </div>
                    </div>
                    <div className="l-2 m-2 c-2 myOrder_orderDetail-productInfo-price">
                        <span style={{ color: "#fe9614" }}>$</span>
                        <span style={{ color: "#fe9614" }}>{orderDetail?.product.sellPrice}</span>
                    </div>
                </div>
                <div className="myOrder_totalPayment">
                    <div>
                        <span style={{ marginRight: "20px" }}>Total payment:</span>
                        <span style={{ fontSize: "2rem", color: "#fe9614" }}>$</span>
                        <span style={{ fontSize: "2rem", color: "#fe9614" }}>{(orderDetail?.sellPrice) * (orderDetail?.quantity)}</span>
                    </div>
                    <div className="myOrder_button-wrap">
                        {/* <button className='myOrder_button-reOrder'>Repurchase</button> */}
                        <ButtonReview />
                    </div>
                </div>
            </div>
        )
    }

    return (
        <>
            <div className='profile__container'>
                <div className="profile__nav">
                    <div className="row profile__nav-wrap">
                        <Link to="/" className="checkout_nav-cart">
                            <b>Home</b>
                        </Link>
                        <div className="detail-nav-icon">
                            <MdKeyboardDoubleArrowRight />
                        </div>
                        <div className="cart_nav-my-orders">
                            <b>My orders</b>
                        </div>
                    </div>
                </div>
                <div className='profile_container'>
                    <div className="row ">
                        <div className="l-2 m-3 c-12 profile_container-sidebar">
                            <div className="leftSide_menu">
                                <div className="leftSide_menu-header">
                                    {username}
                                </div>
                                <div className="leftSide_menu-body">
                                    <div className='leftSide_menu-body-item'>
                                        <FaRegUser />
                                        <Link to="/profile">My account</Link>
                                    </div>
                                    <div className='leftSide_menu-body-item'>
                                        <GrDocumentText />
                                        <Link style={{ color: "#fe9614" }} to={`/my-order?id=${userId}`}>My orders</Link>
                                    </div>
                                    <div className='leftSide_menu-body-item'>
                                        <FiLock />
                                        <Link to={`/change-password?id=${userId}`}>Change password</Link>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="l-10 m-9 c-12 profile_container-content">
                            <div className="myOrder_content-wrap">
                                <div className="profile_content-header">
                                    <h3>My orders</h3>
                                </div>

                                <div className="myOrder_content-body">

                                    {order?.map((item) => (
                                        item.orderDetails.map(orderDetailItem => (
                                            <DisplayOrder orderDate={item.orderDate} orderDetail={orderDetailItem} orderId={item.id} key={orderDetailItem.id} />
                                        ))
                                    ))
                                    }
                                </div>
                            </div>
                            {/* <div>
                                12345
                            </div> */}
                        </div>
                    </div>
                </div>
            </div >
        </>
    )
}

export default MyOrder;