import React, { useEffect, useState } from 'react';
import { ValidationError, number, object, string } from 'yup';
import { FaStar } from 'react-icons/fa';
import { FaArrowLeftLong } from "react-icons/fa6";
import './Grid.css';
import './Payment.css';
import './Profile.css';
import './Review.css';
import { useNavigate } from 'react-router-dom';
import { showToastErrorMessage, showToastWarningMessage } from './ChangePassword';
import { showToastSuccessMessage } from './ProductItem';
import { ToastContainer } from 'react-toastify';
interface Photo {
    id: number;
    name: string;
    originalName: string;
    url: string;
}

interface ProductLine {
    id: number;
    type: string;
    name: string;
}

export interface File {
    id: number;
    name: string;
    originalName: string;
    url: string;
}

// ua co dang xem kv??co, chang hieu j. 

// interface FileList {
//     files: File[] | null | Blob
// } 

// type FileList no co san. hong can khai bao.
// sua load photo nhanh len
// hoc di roi hieu. qq. 
// k phai loi hoc tro ma la loi giao vien chi reactjs 
// doc docs chua? loi gi?doc quickstart no lam j chi may cai type nay???????
// k thich thi xoa type.
//dau phai thich hay k thich ma la loi chay k dc
// loi gi? thi cu bao loi k phai type null  j j do r type string j j do
// input text value = string, di truyen array thi no loiasdjkjkjkajsdkjakldsj
interface OrderDetail {
    id?: number;
    product: Product;
    quantity: number;
    sellPrice: number;
    reviewStatus: boolean;
}

interface Product {
    id: number;
    originalPrice: number;
    productCode: string;
    productDescription: string;
    productName: string;
    quantityInStock: number;
    rate: number;
    brand: ProductLine;
    category: ProductLine
    color: ProductLine;
    sellPrice: number;
    statusPrice: number;
    photos: File[];
    status: string;
    productGuarantee: string;
}

const API_URL = `${process.env.REACT_APP_API_URL}/photos/`
const API_METHOD = 'POST'
const STATUS_IDLE = 0
const STATUS_UPLOADING = 1

interface LoadPhotoProps {
    file: File
}

function Review() {
    const navigate = useNavigate();

    const [files, setFiles] = useState([]);
    const [rating, setRating] = useState<number>(0);
    const [hover, setHover] = useState<number>(0);
    const [photoList, setPhotoList] = useState<File[]>([]);
    const [status, setStatus] = React.useState(STATUS_IDLE)
    const [product, setProduct] = useState<Product | null>(null);
    const [reviewText, setReviewText] = useState('');
    const [style, setStyle] = useState('');
    const [orderDetail, setOrderDetail] = useState(null);

    const searchParams = new URLSearchParams(window.location.search)
    const userId = localStorage.getItem('userId');
    const productId = Number(searchParams.get('productId'))
    const orderId = Number(searchParams.get('orderId'));
    const orderDetailId = Number(searchParams.get('orderDetail'));
    const username = localStorage.getItem('username');

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(
                        `This is an HTTP error: The status is ${response.status}`
                    );
                }
                return response.json();
            })
            .then((actualData) => {
                setProduct(actualData)
            })
    }, []);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/order-detail/${orderDetailId}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error(
                        `This is an HTTP error: The status is ${response.status}`
                    );
                }
                return response.json();
            })
            .then((actualData) => {
                setOrderDetail(actualData);
            })
    }, []);

    // Hàm hiển thị hình 
    function LoadPhoto(props: LoadPhotoProps) {
        const { file } = props;
        return (
            <div className="render-photo d-flex justify-content-between mt-3">
                <img key={file.id} src={file.url} alt="review" />
                <button data-id={file.id} onClick={(e) => deletePhoto(e)} className="render-photo-delete" >X</button>
            </div>
        )
    }

    //Hàm xóa hình
    function deletePhoto(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
        const id = Number(event.currentTarget.dataset.id);

        const newPhotoList = photoList.filter((photo: { id: number; }) => photo.id !== id);
        setPhotoList(newPhotoList);
    };

    const uploadFiles = (files: FileList | null) => {

        if (files != null) {
            const data = new FormData();
            for (let index = 0; index < files.length; index++) {
                data.append(`photos`, files[index])
            }
            setStatus(STATUS_UPLOADING);

            fetch(API_URL, {
                method: API_METHOD,
                body: data,
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem('token')}`,
                },
            })
                .then((res) => res.json())
                .then((data) => {
                    const newPhotoList = setPhotoList(photoList.concat(data));
                })
                .catch((error) => {
                    showToastErrorMessage('Can not upload file');
                })
                .finally(() => setStatus(STATUS_IDLE))
        }
    };

    function sendReview() {
        orderDetail.reviewStatus = true;
        try {
            const response = fetch(`${process.env.REACT_APP_API_URL}/order-detail/${orderDetailId}`, {
                method: "PUT",
                headers: {
                    "Authorization": `Bearer ${localStorage.getItem('token')}`,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(orderDetail),
            });
        } catch (error) {
            const errorString = JSON.stringify(error);
            showToastErrorMessage(errorString);
        }

        const reviewData = {
            product: Number(productId),
            customer: username,
            reviewText: reviewText,
            orderID: orderId,
            rate: rating,
        }

        function validateData() {
            setStyle('');
            let isValid = true;

            if (rating === null) {
                showToastWarningMessage('Please choose rate')
                isValid = false;
            }

            return isValid;
        }

        if (validateData()) {
            try {
                const response = fetch(`${process.env.REACT_APP_API_URL}/review/`, {
                    method: "POST",
                    headers: {
                        "Authorization": `Bearer ${localStorage.getItem('token')}`,
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(reviewData),
                });
                showToastSuccessMessage('You review successful')
                // navigate(`/my-order?id=${userId}`)
            } catch (error) {
                const errorString = JSON.stringify(error);
                showToastErrorMessage(errorString);
            }
        }
    }

    return (
        <>
            <div className='review__container'>
                <div id="modal-review" className="modal">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header row">
                                <div className='l-3 m-3 c-3' style={{ display: "flex", marginLeft: "30px" }}>
                                    <button onClick={() => navigate(`/my-order?id=${userId}`)}
                                        className='modal-header-icon'>
                                        <FaArrowLeftLong className='header-icon' />
                                    </button>
                                </div>
                                <div className='l-6 m-6 c-6' style={{ display: "flex", justifyContent: "center" }}>
                                    <h3 className="modal-title">Review product</h3>
                                </div>
                                <div className="l-3 m-3 c-3"></div>
                            </div>
                            <div className="modal-body">
                                <div className="row review-product_info">
                                    <div className="l-1 review-product_image">
                                        <img src={product?.photos[0].url} alt="" />
                                    </div>
                                    <div className="l-8 review-product_name">
                                        {product?.productName}
                                    </div>
                                </div>
                                <div className='review-product_rate'>
                                    {[...Array(5)].map((star, index) => {
                                        const currentRating = index + 1;
                                        return (
                                            <label key={index}>
                                                <input type="radio" name='rating'
                                                    value={currentRating}
                                                    onClick={() => setRating(currentRating)} />
                                                <FaStar className='star' size={50}
                                                    color={currentRating <= (hover || rating) ? "#ffc107" : "#e4e5e9"}
                                                    onMouseEnter={() => setHover(currentRating)}
                                                    onMouseLeave={() => setHover(0)} />
                                            </label>
                                        )
                                    })}
                                </div>
                                <div className='review-product_rate-text'>
                                    <p>Your rating is {rating}</p>
                                </div>
                                <div className='review-product_image'>
                                    <input id="input-photo" value={JSON.stringify(photoList)} name="photo" type="text" hidden onChange={(e) => (e.target.value)} />
                                    <input id="input-photo-file"
                                        type="file"
                                        accept="image/*"
                                        multiple
                                        onChange={(e) => uploadFiles(e.target.files)} />

                                    <div id="photo-container">
                                        {
                                            photoList.map((photo) => (<LoadPhoto file={photo} key={photo.id} />))
                                        }
                                    </div>
                                </div>
                                <div className='review-product_text'
                                >
                                    <textarea className={`review-product_textarea ${style}`} placeholder='Enter your review' value={reviewText} rows={7} name="review" onChange={e => setReviewText(e.target.value)} id="">
                                    </textarea>
                                </div>
                                <div className='review-product_send'>
                                    <button onClick={sendReview}>Send</button>
                                    <ToastContainer />
                                </div>
                            </div>
                        </div>
                    </div>
                </div >
            </div >
        </>
    )
}

export default Review;