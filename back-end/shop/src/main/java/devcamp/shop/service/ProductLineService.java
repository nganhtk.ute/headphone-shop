package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CProductLine;
import devcamp.shop.repository.IProductLineRepository;

@Service
public class ProductLineService {

    @Autowired
    IProductLineRepository productLineRepository;

    public List<CProductLine> getProductLines() {
        return productLineRepository.findAll();
    }

    public CProductLine getProductLineById(int id) {
        return productLineRepository.findById(id).get();
    }

    public CProductLine createProductLine(CProductLine nProductLine) {
        CProductLine savedProductLine = productLineRepository.save(nProductLine);
        return savedProductLine;
    }

    public CProductLine updateProductLine(int id,
            CProductLine nProductLine) {
        CProductLine oldProductLine = getProductLineById(id);
        oldProductLine.setName(nProductLine.getName());
        oldProductLine.setType(nProductLine.getType());

        return productLineRepository.save(oldProductLine);
    }

    public void deleteProductLine(int id) {
        CProductLine oldProductLine = getProductLineById(id);
        productLineRepository.deleteById(oldProductLine.getId());
    }
}
