package devcamp.shop.service;

import java.util.List;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import devcamp.shop.model.CCustomer;

public class ExcelExporter<T> {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;

    private String sheetName;
    private List<T> records;
    private List<String> headers;
    private List<String> columns;

    public ExcelExporter(List<T> records, List<String> headers, List<String> columns, String sheetName) {
        this.records = records;
        this.headers = headers;
        this.columns = columns;
        this.sheetName = sheetName;

        this.workbook = new XSSFWorkbook();
    }

    /**
     * Tạo các ô cho excel file.
     * 
     * @param row
     * @param columnCount
     * @param value
     * @param style
     */
    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Timestamp) {
            cell.setCellValue((String) new SimpleDateFormat("dd-MM-yyyy").format(value));
        } else if (value instanceof CCustomer) {
            cell.setCellValue((String) ((CCustomer) value).getFullname());
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    /**
     * Khai báo cho sheet và các dòng đầu tiên
     */
    private void writeHeaderLine() {
        sheet = workbook.createSheet(sheetName);

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);
        style.setFillBackgroundColor(IndexedColors.BRIGHT_GREEN.getIndex());
        int index = 0;

        for (String header : headers) {
            createCell(row, index++, header, style);
        }
    }

    /**
     * fill dữ liệu cho các dòng tiếp theo.
     */
    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (T user : this.records) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            for (String column : this.columns) {
                try {
                    createCell(row, columnCount++, user.getClass().getDeclaredMethod(column).invoke(user), style);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException
                        | NoSuchMethodException | SecurityException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * xuất dữ liệu ra dạng file
     * 
     * @param response
     * @throws IOException
     */
    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}