package devcamp.shop.service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

import org.hibernate.mapping.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CCustomer;
import devcamp.shop.model.CDistrict;
import devcamp.shop.model.COrder;
import devcamp.shop.model.COrderDetail;
import devcamp.shop.model.CProvince;
import devcamp.shop.model.CStreet;
import devcamp.shop.model.CWard;
import devcamp.shop.repository.ICustomerRepository;
import devcamp.shop.repository.IDistrictRepository;
import devcamp.shop.repository.IOrderDetailRepository;
import devcamp.shop.repository.IOrderRepository;
import devcamp.shop.repository.IProvinceRepository;
import devcamp.shop.repository.IStreetRepository;
import devcamp.shop.repository.IWardRepository;

@Service
public class OrderService {

    @Autowired
    IOrderRepository orderRepository;

    @Autowired
    IOrderDetailRepository orderDetailRepository;

    @Autowired
    ICustomerRepository customerRepository;

    @Autowired
    CustomerService customerService;

    @Autowired
    IWardRepository wardRepository;

    @Autowired
    IProvinceRepository provinceRepository;

    @Autowired
    IDistrictRepository districtRepository;

    @Autowired
    IStreetRepository streetRepository;

    public List<COrder> getOrders() {
        return orderRepository.findAll();
    }

    public COrder getOrderById(int id) {
        return orderRepository.findById(id).get();
    }

    public COrder createOrder(COrder nOrder) {
        nOrder.setOrderDate(new Date());

        if (nOrder.getProvince() != null) {
            CProvince province = provinceRepository.findById(nOrder.getProvince().getId()).get();
            nOrder.setProvince(province);
        }

        if (nOrder.getDistrict() != null) {
            CDistrict district = districtRepository.findById(nOrder.getDistrict().getId()).get();
            nOrder.setDistrict(district);
        }

        if (nOrder.getWard() != null) {
            CWard ward = wardRepository.findById(nOrder.getWard().getId()).get();
            nOrder.setWard(ward);
        }

        if (nOrder.getStreet() != null) {
            CStreet street = streetRepository.findById(nOrder.getStreet().getId()).get();
            nOrder.setStreet(street);
        }

        nOrder.getOrderDetails().forEach(orderDetail -> {
            orderDetail.setOrder(nOrder);
        });

        COrder savedOrder = orderRepository.save(nOrder);
        return savedOrder;
    }

    public COrder updateOrder(int id,
            COrder nOrder) {
        COrder oldOrder = getOrderById(id);
        oldOrder.setCustomer(nOrder.getCustomer());
        oldOrder.setOrderDate(new Date());
        oldOrder.setShippedDate(nOrder.getShippedDate());
        oldOrder.setReceiverName(nOrder.getReceiverName());
        oldOrder.setReceiverPhone(nOrder.getReceiverPhone());
        oldOrder.setNote(nOrder.getNote());
        oldOrder.setPaymentMethod(nOrder.getPaymentMethod());

        if (nOrder.getProvince() != null) {
            CProvince province = provinceRepository.findById(nOrder.getProvince().getId()).get();
            oldOrder.setProvince(province);
        } else {
            oldOrder.setProvince(null);
        }

        if (nOrder.getDistrict() != null) {
            CDistrict district = districtRepository.findById(nOrder.getDistrict().getId()).get();
            oldOrder.setDistrict(district);
        } else {
            oldOrder.setDistrict(null);
        }

        if (nOrder.getWard() != null) {
            CWard ward = wardRepository.findById(nOrder.getWard().getId()).get();
            oldOrder.setWard(ward);
        } else {
            oldOrder.setWard(null);
        }

        if (nOrder.getStreet() != null) {
            CStreet street = streetRepository.findById(nOrder.getStreet().getId()).get();
            oldOrder.setStreet(street);
        } else {
            oldOrder.setStreet(null);
        }

        oldOrder.getOrderDetails().forEach(orderDetail -> {
            orderDetail.setOrder(null);
        });
        oldOrder.getOrderDetails().clear();

        nOrder.getOrderDetails().forEach(orderDetail -> {
            orderDetail.setOrder(oldOrder);
        });
        oldOrder.getOrderDetails().addAll(nOrder.getOrderDetails());

        return orderRepository.save(oldOrder);
    }

    public void deleteOrder(int id) {
        COrder oldOrder = getOrderById(id);
        orderRepository.deleteById(oldOrder.getId());
    }

}
