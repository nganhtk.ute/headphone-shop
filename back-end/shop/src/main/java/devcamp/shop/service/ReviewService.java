package devcamp.shop.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CCustomer;
import devcamp.shop.model.CProduct;
import devcamp.shop.model.CReview;
import devcamp.shop.repository.ICustomerRepository;
import devcamp.shop.repository.IProductRepository;
import devcamp.shop.repository.IReviewRepository;

@Service
public class ReviewService {

    @Autowired
    IReviewRepository reviewRepository;

    @Autowired
    ICustomerRepository customerRepository;

    @Autowired
    IProductRepository productRepository;

    public List<CReview> getReviews() {
        return reviewRepository.findAll();
    }

    public CReview getReviewById(int id) {
        return reviewRepository.findById(id).get();
    }

    public CReview createReview(CReview nReview) {
        nReview.setCreateDate(new Date());
        CReview savedReview = reviewRepository.save(nReview);
        return savedReview;
    }

    public CReview updateReview(int id,
            CReview nReview) {
        CReview oldReview = getReviewById(id);
        oldReview.setCustomer(nReview.getCustomer());
        oldReview.setProduct(nReview.getProduct());
        oldReview.setRate(nReview.getRate());
        oldReview.setReviewText(nReview.getReviewText());
        oldReview.setOrderID(nReview.getOrderID());

        return reviewRepository.save(oldReview);
    }

    public void deleteReview(int id) {
        CReview oldReview = getReviewById(id);
        reviewRepository.deleteById(oldReview.getId());
    }
}
