package devcamp.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.login.Token;
import devcamp.shop.repository.TokenRepository;

@Service
public class TokenService {

    @Autowired
    TokenRepository tokenRepository;

    public Token findByToken(String token) {
        return tokenRepository.findByToken(token);
    }

    public Token createToken(Token nToken) {
        Token savedToken = tokenRepository.save(nToken);
        return savedToken;
    }

    public void deleteByToken(String token) {
        tokenRepository.deleteByToken(token);
    }
}
