package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CDistrict;
import devcamp.shop.model.CProvince;
import devcamp.shop.repository.IDistrictRepository;
import devcamp.shop.repository.IWardRepository;

@Service
public class DistrictService {
    @Autowired
    ProvinceService provinceService;

    @Autowired
    IDistrictRepository districtRepository;

    @Autowired
    IWardRepository wardRepository;

    public List<CDistrict> getDistricts() {
        return districtRepository.findAll();
    }

    public List<CDistrict> findByProvinceId(Integer id) {
        return districtRepository.findByProvinceId(id);
    }

    public CDistrict getDistrictById(int id) {
        return districtRepository.findById(id).get();
    }

    public CDistrict createDistrict(CDistrict nDistrict) {
        CProvince province = provinceService.getProvinceById(nDistrict.getProvince().getId());
        nDistrict.setProvince(province);

        return districtRepository.save(nDistrict);
    }

    public CDistrict updateDistrict(int id, CDistrict nDistrict) {
        CDistrict oldDistrict = getDistrictById(id);
        oldDistrict.setDistrictName(nDistrict.getDistrictName());
        oldDistrict.setPrefix(nDistrict.getPrefix());

        CProvince province = provinceService.getProvinceById(nDistrict.getProvince().getId());
        oldDistrict.setProvince(province);

        return districtRepository.save(oldDistrict);
    }

    public void deleteDistrict(int id) {
        CDistrict oldDistrict = getDistrictById(id);
        districtRepository.deleteById(oldDistrict.getId());
    }
}
