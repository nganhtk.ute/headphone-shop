package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CStreet;
import devcamp.shop.model.CDistrict;
import devcamp.shop.repository.IStreetRepository;

@Service
public class StreetService {
    @Autowired
    DistrictService districtService;

    @Autowired
    IStreetRepository streetRepository;

    public List<CStreet> getStreets() {
        return streetRepository.findAll();
    }

    public List<CStreet> findByDistrictId(Integer id) {
        return streetRepository.findByDistrictId(id);
    }

    public CStreet getStreetById(int id) {
        return streetRepository.findById(id).get();
    }

    public CStreet createStreet(CStreet nStreet) {
        CDistrict district = districtService.getDistrictById(nStreet.getDistrict().getId());
        nStreet.setDistrict(district);

        return streetRepository.save(nStreet);
    }

    public CStreet updateStreet(int id, CStreet nStreet) {
        CStreet oldStreet = getStreetById(id);
        oldStreet.setStreetName(nStreet.getStreetName());
        oldStreet.setPrefix(nStreet.getPrefix());

        CDistrict district = districtService.getDistrictById(nStreet.getDistrict().getId());
        nStreet.setDistrict(district);

        return streetRepository.save(oldStreet);
    }

    public void deleteStreet(int id) {
        CStreet oldStreet = getStreetById(id);
        streetRepository.deleteById(oldStreet.getId());
    }
}
