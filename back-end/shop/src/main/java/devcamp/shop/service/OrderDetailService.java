package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.COrderDetail;
import devcamp.shop.repository.IOrderDetailRepository;

@Service
public class OrderDetailService {

    @Autowired
    IOrderDetailRepository orderDetailRepository;

    public List<COrderDetail> getOrderDetails() {
        return orderDetailRepository.findAll();
    }

    public COrderDetail getOrderDetailById(int id) {
        return orderDetailRepository.findById(id).get();
    }

    public COrderDetail createOrderDetail(COrderDetail nOrder) {
        COrderDetail savedOrder = orderDetailRepository.save(nOrder);
        return savedOrder;
    }

    public COrderDetail updateOrderDetail(int id,
            COrderDetail nOrderDetail) {
        COrderDetail oldOrderDetail = getOrderDetailById(id);
        oldOrderDetail.setOrder(nOrderDetail.getOrder());
        oldOrderDetail.setProduct(nOrderDetail.getProduct());
        oldOrderDetail.setQuantity(nOrderDetail.getQuantity());
        oldOrderDetail.setSellPrice(nOrderDetail.getSellPrice());
        oldOrderDetail.setReviewStatus(nOrderDetail.isReviewStatus());
        return orderDetailRepository.save(oldOrderDetail);
    }

    public void deleteOrderDetail(int id) {
        COrderDetail oldOrderDetail = getOrderDetailById(id);
        orderDetailRepository.deleteById(oldOrderDetail.getId());
    }
}
