package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CProduct;
import devcamp.shop.model.CProductLine;
import devcamp.shop.repository.IProductLineRepository;
import devcamp.shop.repository.IProductRepository;

@Service
public class ProductService {

    @Autowired
    IProductRepository productRepository;

    @Autowired
    IProductLineRepository productLineRepository;

    public List<CProduct> getProducts() {
        return productRepository.findAll();
    }

    public CProduct getProductById(int id) {
        return productRepository.findById(id).get();
    }

    public CProduct createProduct(CProduct nProduct) {
        if (nProduct.getBrand() != null) {
            CProductLine brand = productLineRepository.findById(nProduct.getBrand().getId()).get();

            nProduct.setBrand(brand);
        }

        if (nProduct.getCategory() != null) {
            CProductLine category = productLineRepository.findById(nProduct.getCategory().getId()).get();

            nProduct.setCategory(category);
        }

        if (nProduct.getColor() != null) {
            CProductLine color = productLineRepository.findById(nProduct.getColor().getId()).get();

            nProduct.setColor(color);
        }

        CProduct savedProduct = productRepository.save(nProduct);
        return savedProduct;
    }

    public CProduct updateProduct(int id,
            CProduct nProduct) {
        CProduct oldProduct = getProductById(id);
        oldProduct.setBrand(nProduct.getBrand());
        oldProduct.setCategory(nProduct.getCategory());
        oldProduct.setColor(nProduct.getColor());
        oldProduct.setOriginalPrice(nProduct.getOriginalPrice());
        oldProduct.setProductCode(nProduct.getProductCode());
        oldProduct.setProductDescription(nProduct.getProductDescription());
        oldProduct.setProductName(nProduct.getProductName());
        oldProduct.setQuantityInStock(nProduct.getQuantityInStock());
        oldProduct.setSellPrice(nProduct.getSellPrice());
        oldProduct.setProductGuarantee(nProduct.getProductGuarantee());

        return productRepository.save(oldProduct);
    }

    public void deleteProduct(int id) {
        CProduct oldProduct = getProductById(id);
        productRepository.deleteById(oldProduct.getId());
    }
}
