package devcamp.shop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CFile;
import devcamp.shop.repository.IFileRepository;

@Service
public class FileService {

    @Autowired
    IFileRepository fileRepository;

    public CFile getFileById(int id) {
        return fileRepository.findById(id).get();
    }

    public CFile createFile(CFile nFile) {
        return fileRepository.save(nFile);
    }

    public void deleteFile(int id) {
        CFile oldFile = getFileById(id);
        fileRepository.delete(oldFile);
    }
}
