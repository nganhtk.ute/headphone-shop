package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CDistrict;
import devcamp.shop.model.CEmployee;
import devcamp.shop.model.CProvince;
import devcamp.shop.model.CStreet;
import devcamp.shop.model.CWard;
import devcamp.shop.repository.IDistrictRepository;
import devcamp.shop.repository.IEmployeeRepository;
import devcamp.shop.repository.IProvinceRepository;
import devcamp.shop.repository.IStreetRepository;
import devcamp.shop.repository.IWardRepository;

@Service
public class EmployeeService {
    @Autowired
    IEmployeeRepository employeeRepository;

    @Autowired
    IWardRepository wardRepository;

    @Autowired
    IProvinceRepository provinceRepository;

    @Autowired
    IDistrictRepository districtRepository;

    @Autowired
    IStreetRepository streetRepository;

    public List<CEmployee> getEmployees() {
        return employeeRepository.findAll();
    }

    public CEmployee getEmployeeById(Integer id) {
        return employeeRepository.findById(id).get();
    }

    public CEmployee createEmployee(CEmployee nEmployee) {
        if (nEmployee.getProvince() != null) {
            CProvince province = provinceRepository.findById(nEmployee.getProvince().getId()).get();
            nEmployee.setProvince(province);
        }

        if (nEmployee.getDistrict() != null) {
            CDistrict district = districtRepository.findById(nEmployee.getDistrict().getId()).get();
            nEmployee.setDistrict(district);
        }

        if (nEmployee.getWard() != null) {
            CWard ward = wardRepository.findById(nEmployee.getWard().getId()).get();
            nEmployee.setWard(ward);
        }

        if (nEmployee.getStreet() != null) {
            CStreet street = streetRepository.findById(nEmployee.getStreet().getId()).get();
            nEmployee.setStreet(street);
        }

        return employeeRepository.save(nEmployee);
    }

    public CEmployee updateEmployee(Integer id, CEmployee newInfo) {
        CEmployee oldEmployee = getEmployeeById(id);
        oldEmployee.setUsername(newInfo.getUsername());
        oldEmployee.setPassword(newInfo.getPassword());
        oldEmployee.setBirthDate(newInfo.getBirthDate());
        oldEmployee.setEmail(newInfo.getEmail());
        oldEmployee.setHireDate(newInfo.getHireDate());
        oldEmployee.setPhone(newInfo.getPhone());
        oldEmployee.setJobTitle(newInfo.getJobTitle());
        oldEmployee.setPhotos(newInfo.getPhotos());
        oldEmployee.setReportsTo((newInfo.getReportsTo()));

        if (newInfo.getProvince() != null) {
            CProvince province = provinceRepository.findById(newInfo.getProvince().getId()).get();
            oldEmployee.setProvince(province);
        } else {
            oldEmployee.setProvince(null);
        }

        if (newInfo.getDistrict() != null) {
            CDistrict district = districtRepository.findById(newInfo.getDistrict().getId()).get();
            oldEmployee.setDistrict(district);
        } else {
            oldEmployee.setDistrict(null);
        }

        if (newInfo.getWard() != null) {
            CWard ward = wardRepository.findById(newInfo.getWard().getId()).get();
            oldEmployee.setWard(ward);
        } else {
            oldEmployee.setWard(null);
        }

        if (newInfo.getStreet() != null) {
            CStreet street = streetRepository.findById(newInfo.getStreet().getId()).get();
            oldEmployee.setStreet(street);
        } else {
            oldEmployee.setStreet(null);
        }
        // if (newInfo.getReportsTo() != null
        // && newInfo.getReportsTo(). != newInfo.getUser()) {
        // CEmployee employee = getEmployeeById(newInfo.getReportsTo().getUser());
        // oldEmployee.setReportsTo(employee);
        // } else {
        // oldEmployee.setReportsTo(null);
        // }

        return employeeRepository.save(oldEmployee);
    }

    public void deleteEmployee(Integer id) {
        CEmployee oldEmployee = getEmployeeById(id);
        employeeRepository.delete(oldEmployee);
    }
}
