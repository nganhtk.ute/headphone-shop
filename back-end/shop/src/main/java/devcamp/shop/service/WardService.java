package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CWard;
import devcamp.shop.model.CDistrict;
import devcamp.shop.repository.IWardRepository;

@Service
public class WardService {
    @Autowired
    DistrictService districtService;

    @Autowired
    IWardRepository wardRepository;

    public List<CWard> getWards() {
        return wardRepository.findAll();
    }

    public List<CWard> findByDistrictId(Integer id) {
        return wardRepository.findByDistrictId(id);
    }

    public CWard getWardById(int id) {
        return wardRepository.findById(id).get();
    }

    public CWard createWard(CWard nWard) {
        CDistrict district = districtService.getDistrictById(nWard.getDistrict().getId());
        nWard.setDistrict(district);

        return wardRepository.save(nWard);
    }

    public CWard updateWard(int id, CWard nWard) {
        CWard oldWard = getWardById(id);
        oldWard.setWardName(nWard.getWardName());
        oldWard.setPrefix(nWard.getPrefix());

        CDistrict district = districtService.getDistrictById(nWard.getDistrict().getId());
        nWard.setDistrict(district);

        return wardRepository.save(oldWard);
    }

    public void deleteWard(int id) {
        CWard oldWard = getWardById(id);
        wardRepository.deleteById(oldWard.getId());
    }
}
