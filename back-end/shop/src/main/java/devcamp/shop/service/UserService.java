package devcamp.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import devcamp.shop.model.CUser;
import devcamp.shop.repository.IUserRepository;

@Service
public class UserService {
    @Autowired
    IUserRepository userRepository;

    public List<CUser> getUsers() {
        return userRepository.findAll();
    }

    public CUser getUserById(int id) {
        return userRepository.findById(id).get();
    }

    public CUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public CUser createUser(CUser nUser) {
        CUser savedUser = userRepository.save(nUser);
        return savedUser;
    }

    public CUser updateUser(int id,
            CUser nUser) {
        CUser oldUser = getUserById(id);
        oldUser.setPassword(nUser.getPassword());
        oldUser.setUsername(nUser.getUsername());

        return userRepository.save(oldUser);
    }

    public void deleteUser(int id) {
        CUser oldUser = getUserById(id);
        userRepository.deleteById(oldUser.getId());
    }
}
