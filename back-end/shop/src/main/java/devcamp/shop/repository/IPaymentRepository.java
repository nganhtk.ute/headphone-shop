package devcamp.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.shop.model.CPayment;

public interface IPaymentRepository extends JpaRepository<CPayment, Integer> {

}
