package devcamp.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.shop.model.CProvince;

public interface IProvinceRepository extends JpaRepository<CProvince, Integer> {

}
