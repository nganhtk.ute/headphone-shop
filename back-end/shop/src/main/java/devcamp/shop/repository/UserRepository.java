package devcamp.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.shop.login.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
