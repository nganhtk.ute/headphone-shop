package devcamp.shop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.shop.model.CEmployee;

public interface IEmployeeRepository extends JpaRepository<CEmployee, Integer> {
}
