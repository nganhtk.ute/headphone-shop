package devcamp.shop.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import devcamp.shop.model.CWard;

public interface IWardRepository extends JpaRepository<CWard, Integer> {
    List<CWard> findByDistrictId(int id);

    @Query(value = """
            FROM CWard w
            WHERE (:province IS NULL OR :province = w.district.province.id)
                AND (:district IS NULL OR :district = w.district.id)
                AND (:keyword IS NULL OR LOWER(w.wardName) LIKE CONCAT('%', LOWER(:keyword), '%') OR LOWER(w.district.districtName) LIKE CONCAT('%', LOWER(:keyword), '%') OR LOWER(w.district.province.provinceName) LIKE CONCAT('%', LOWER(:keyword), '%'))
                    """)
    public Page<CWard> findWithPaging(
            Optional<Integer> province,
            Optional<Integer> district,
            Optional<String> keyword,
            Pageable pageable);
}
