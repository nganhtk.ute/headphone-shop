package devcamp.shop.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import devcamp.shop.model.COrderDetail;

public interface IOrderDetailRepository extends JpaRepository<COrderDetail, Integer> {
        @Query(value = """
                        FROM COrderDetail r
                        WHERE :order IS NULL OR :order = r.order.id
                        AND(:product IS NULL OR :product = r.product.id)
                                """)
        public Page<COrderDetail> findOrderDetailByRequest(
                        Optional<Integer> order,
                        Optional<Integer> product,
                        Pageable pageable);
}
