package devcamp.shop.repository;

import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import devcamp.shop.model.CReview;

public interface IReviewRepository extends JpaRepository<CReview, Integer> {

        @Query(value = """
                        FROM CReview r
                        WHERE (:product IS NULL OR r.product = :product)
                        """)
        public Set<CReview> findReviewByProductId(
                        Optional<Integer> product);

        @Query(value = """
                        SELECT *
                        FROM review r
                        WHERE (:product IS NULL OR r.product = :product)
                        AND(:customer IS NULL OR r.customer = :customer)
                        AND(:orderID IS NULL OR r.orderID = :orderID)
                        AND (:rate IS NULL OR r.rate = :rate)
                        AND (r.review_text REGEXP :keyword)
                        """, nativeQuery = true)

        public Page<CReview> findReviewByRequest(
                        Optional<Integer> product,
                        Optional<Integer> rate,
                        Optional<String> customer,
                        Optional<Integer> orderID,
                        String keyword,
                        Pageable pageable);

        @Query(value = """
                        FROM CReview r
                        WHERE (:product IS NULL OR r.product = :product)
                        AND(r.reviewText != '')
                        """)
        public Page<CReview> findReviewByReviewText(Optional<Integer> product, Pageable pageable);

}
