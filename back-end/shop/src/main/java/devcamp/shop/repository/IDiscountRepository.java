package devcamp.shop.repository;

import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import devcamp.shop.model.CDiscount;

public interface IDiscountRepository extends JpaRepository<CDiscount, Integer> {
        @Query(value = """
                        FROM CDiscount r
                        WHERE (:keyword IS NULL OR LOWER(r.discountCode) LIKE CONCAT('%',LOWER(:keyword), '%'))
                        """)
        public Page<CDiscount> findDiscountByRequest(
                        Optional<String> keyword,
                        Pageable pageable);

        @Query(value = """
                        FROM CDiscount r
                        WHERE r.discountCode = :keyword
                        """)
        public CDiscount findDiscountByKeyword(
                        Optional<String> keyword);

}
