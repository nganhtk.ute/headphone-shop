-- doanh thu theo ngay
SELECT DATE(o.order_date) as date, SUM(od.quantity * od.sell_price) as `revenue`
FROM `orders` o
JOIN `order_detail` od ON o.id = od.order_id
WHERE o.`order_date` BETWEEN '2022-11-21 00:00:00' and '2024-11-21 23:59:59'
GROUP BY DATE(o.order_date);


-- doanh thu theo ngay trong tuan
SELECT DATE(o.order_date) as date, DAYNAME(o.order_date) as weekday, SUM(od.quantity * od.sell_price) as `revenue`
FROM `orders` o
JOIN `order_detail` od ON o.id = od.order_id
WHERE o.`order_date` BETWEEN '2023-11-20 00:00:00' and '2023-11-26 23:59:59'
GROUP BY DAYNAME(o.order_date);

-- doanh thu theo tuan trong thang
SELECT DATE(o.order_date) as date, DAYNAME(o.order_date) as weekday, WEEK(o.order_date, 3) as week, SUM(od.quantity * od.sell_price) as `revenue`
FROM `orders` o
JOIN `order_detail` od ON o.id = od.order_id
WHERE o.`order_date` BETWEEN '2023-11-01 00:00:00' and '2023-11-30 23:59:59'
GROUP BY WEEK(o.order_date, 3);

-- doanh thu theo ngay trong thang
SELECT DATE(o.order_date) as date, DAYNAME(o.order_date) as `day_of_week`, DAYOFMONTH(o.order_date) as `day_of_month`, WEEK(o.order_date, 3) as week, SUM(od.quantity * od.sell_price) as `revenue`
FROM `orders` o
JOIN `order_detail` od ON o.id = od.order_id
WHERE o.`order_date` BETWEEN '2023-11-01 00:00:00' and '2023-11-30 23:59:59'
GROUP BY DAYOFMONTH(o.order_date);

-- doanh thu theo thang trong nam
SELECT DATE(o.order_date) as date, DAYNAME(o.order_date) as `day_of_week`, DAYOFMONTH(o.order_date) as `day_of_month`, WEEK(o.order_date, 3) as week, MONTHNAME(o.order_date) as month, SUM(od.quantity * od.sell_price) as `revenue`
FROM `orders` o
JOIN `order_detail` od ON o.id = od.order_id
WHERE o.`order_date` BETWEEN '2023-11-01 00:00:00' and '2023-11-30 23:59:59'
GROUP BY MONTHNAME(o.order_date);