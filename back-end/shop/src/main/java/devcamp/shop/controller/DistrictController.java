package devcamp.shop.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.CDistrict;
import devcamp.shop.model.CStreet;
import devcamp.shop.model.CWard;
import devcamp.shop.repository.IDistrictRepository;
import devcamp.shop.service.DistrictService;
import devcamp.shop.service.StreetService;
import devcamp.shop.service.WardService;

@CrossOrigin
@RestController
@RequestMapping("/districts")
public class DistrictController {
    @Autowired
    DistrictService districtService;

    @Autowired
    StreetService streetService;

    @Autowired
    WardService wardService;

    @Autowired
    IDistrictRepository districtRepository;

    @GetMapping("/{id}")
    public ResponseEntity<CDistrict> getDistrictByDistrictId(
            @PathVariable("id") int districtId) {
        try {
            CDistrict findDistrict = districtService.getDistrictById(districtId);
            return new ResponseEntity<>(findDistrict, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}/wards")
    public ResponseEntity<List<CWard>> getWardsByDistrictId(@PathVariable("id") int id) {
        try {
            List<CWard> wards = wardService.findByDistrictId(id);
            return new ResponseEntity<>(wards, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}/streets")
    public ResponseEntity<List<CStreet>> getStreetsByDistrictId(@PathVariable("id") int id) {
        try {
            List<CStreet> streets = streetService.findByDistrictId(id);
            return new ResponseEntity<>(streets, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<CDistrict>> getDistricts() {
        try {
            List<CDistrict> lstDistrict = districtService.getDistricts();
            return new ResponseEntity<>(lstDistrict, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging")
    public ResponseEntity<Object> findWithPaging(
            @RequestParam(value = "province", required = false) Optional<Integer> province,
            @RequestParam(value = "keyword", required = false) Optional<String> keyword,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "6") int size,
            @RequestParam(value = "sortBy", defaultValue = "id") String sortBy,
            @RequestParam(value = "sortDirection", defaultValue = "DESC") Direction sortDirection) {
        try {
            return new ResponseEntity<>(districtRepository.findWithPaging(
                    province,
                    keyword,
                    PageRequest.of(page, size, Sort.by(sortDirection, sortBy))), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/province/{id}")
    public ResponseEntity<List<CDistrict>> getDistrictByProvinceId(@PathVariable("id") int provinceId) {
        try {
            List<CDistrict> lstDistrict = districtRepository.findByProvinceId(provinceId);
            return new ResponseEntity<>(lstDistrict, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createDistrict(@Valid @RequestBody CDistrict cDistrict) {
        try {
            CDistrict savedDistrict = districtService.createDistrict(cDistrict);
            return new ResponseEntity<>(savedDistrict, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateDistrict(@PathVariable("id") int id, @Valid @RequestBody CDistrict cDistrict) {
        try {
            CDistrict savedDistrict = districtService.updateDistrict(id, cDistrict);
            return new ResponseEntity<>(savedDistrict, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable int id) {
        try {
            districtService.deleteDistrict(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
