package devcamp.shop.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.COrderDetail;
import devcamp.shop.repository.IOrderDetailRepository;
import devcamp.shop.service.OrderDetailService;

@CrossOrigin
@RestController
@RequestMapping("/order-detail")
public class OrderDetailController {
    @Autowired
    OrderDetailService orderDetailService;

    @Autowired
    IOrderDetailRepository orderDetailRepository;

    @GetMapping("/{id}")
    public ResponseEntity<COrderDetail> getOrderByOrderDetailId(@PathVariable("id") int orderDetailId) {
        try {
            COrderDetail findOrderDetail = orderDetailService.getOrderDetailById(orderDetailId);
            return new ResponseEntity<>(findOrderDetail, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<COrderDetail>> getOrder() {
        try {
            List<COrderDetail> lstOrderDetails = orderDetailService.getOrderDetails();

            return new ResponseEntity<>(lstOrderDetails, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging")
    public ResponseEntity<Object> findWithPaging(
            @RequestParam(value = "order", required = false) Optional<Integer> order,
            @RequestParam(value = "product", required = false) Optional<Integer> product,

            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "6") int size) {
        try {
            return new ResponseEntity<>(orderDetailRepository.findOrderDetailByRequest(
                    order, product,
                    PageRequest.of(page, size)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createOrderDetail(@Valid @RequestBody COrderDetail cOrderDetail) {
        try {
            COrderDetail savedOrderDetail = orderDetailService.createOrderDetail(cOrderDetail);
            return new ResponseEntity<>(savedOrderDetail, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_CUSTOMER')")
    public ResponseEntity<Object> updateOrderDetail(@PathVariable("id") int id,
            @Valid @RequestBody COrderDetail COrderDetail) {
        try {
            COrderDetail savedOrderDetail = orderDetailService.updateOrderDetail(id,
                    COrderDetail);
            return new ResponseEntity<>(savedOrderDetail, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    // @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteOrderDetailById(@PathVariable int id) {
        try {
            orderDetailService.deleteOrderDetail(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
