package devcamp.shop.controller;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.login.Token;
import devcamp.shop.model.CCustomer;
import devcamp.shop.model.CUser;
import devcamp.shop.repository.IUserRepository;
import devcamp.shop.security.JwtUtil;
import devcamp.shop.service.CustomerService;
import devcamp.shop.service.TokenService;
import devcamp.shop.service.UserService;

@CrossOrigin
@RestController
public class AuthController {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private UserService userService;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private JwtUtil jwtUtil;

    @PostMapping("/register")
    public ResponseEntity<Object> register(@Valid @RequestBody CCustomer customer) {
        try {

            customer.setPassword(new BCryptPasswordEncoder().encode(customer.getPassword()));

            CCustomer savedCustomer = customerService.createCustomer(customer);

            return new ResponseEntity<>(savedCustomer, HttpStatus.CREATED);
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>("Username already exist", HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/login-client")
    public ResponseEntity<?> login(@RequestBody CUser user) {
        try {
            CUser foundUser = customerService.findByUsername(user.getUsername());

            if (null == foundUser || !new BCryptPasswordEncoder().matches(user.getPassword(),
                    foundUser.getPassword())) {
                return new ResponseEntity<>("Username or password incorrect", HttpStatus.FORBIDDEN);
            }

            Token token = new Token();
            token.setToken(jwtUtil.generateToken(foundUser));
            token.setTokenExpDate(jwtUtil.generateExpirationDate());
            token.setCreatedBy(Long.valueOf(foundUser.getId()));
            tokenService.createToken(token);

            Map<String, String> response = new HashMap<String, String>();
            response.put("token", token.getToken());
            response.put("username", foundUser.getUsername());
            response.put("id", Integer.toString(foundUser.getId()));
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/login-admin")
    public ResponseEntity<?> loginAdmin(
            @Valid @RequestBody CUser user) {
        try {
            CUser foundUser = userService.findByUsername(user.getUsername());

            if (null == foundUser || !new BCryptPasswordEncoder().matches(user.getPassword(),
                    foundUser.getPassword())) {
                return new ResponseEntity<>("Username or password incorrect", HttpStatus.FORBIDDEN);
            }

            Token token = new Token();
            token.setToken(jwtUtil.generateToken(foundUser));
            token.setTokenExpDate(jwtUtil.generateExpirationDate());
            token.setCreatedBy(Long.valueOf(foundUser.getId()));
            tokenService.createToken(token);

            Map<String, String> response = new HashMap<String, String>();
            response.put("token", token.getToken());
            response.put("username", foundUser.getUsername());
            response.put("id", Integer.toString(foundUser.getId()));
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/change-password")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordBody body,
            @AuthenticationPrincipal CUser currentUser) {
        try {
            if (!new BCryptPasswordEncoder().matches(body.getOldPassword(),
                    currentUser.getPassword())) {
                return new ResponseEntity<>("Old Password incorrect", HttpStatus.BAD_REQUEST);
            }

            currentUser.setPassword(new BCryptPasswordEncoder().encode(body.getNewPassword()));
            userRepository.save(currentUser);

            return new ResponseEntity<>("Update Password successful", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Transactional
    @PostMapping("/log-out")
    public ResponseEntity<Object> logout(Authentication authentication) {
        try {
            if (authentication != null) {
                String token = (String) authentication.getCredentials();
                tokenService.deleteByToken(token);

                SecurityContextHolder.getContext().setAuthentication(null);
            }

            return new ResponseEntity<>(HttpStatus.OK);
        } catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>("Username already exist", HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

class ChangePasswordBody {
    private String oldPassword;

    private String newPassword;

    public ChangePasswordBody() {
    }

    public ChangePasswordBody(String oldPassword, String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
