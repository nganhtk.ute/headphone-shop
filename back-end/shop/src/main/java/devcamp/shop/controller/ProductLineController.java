package devcamp.shop.controller;

import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.CProductLine;
import devcamp.shop.service.ProductLineService;

@CrossOrigin
@RestController
@RequestMapping("/product-line")
public class ProductLineController {
    @Autowired
    ProductLineService productLineService;

    @GetMapping("/{id}")
    public ResponseEntity<CProductLine> getProductByProductLineId(@PathVariable("id") int productLineId) {
        try {
            CProductLine findProductLine = productLineService.getProductLineById(productLineId);
            return new ResponseEntity<>(findProductLine, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<CProductLine>> getProductLine() {
        try {
            List<CProductLine> lstProductLines = productLineService.getProductLines();

            return new ResponseEntity<>(lstProductLines, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createProductLine(@Valid @RequestBody CProductLine cProductLine) {
        try {
            CProductLine savedProductLine = productLineService.createProductLine(cProductLine);
            return new ResponseEntity<>(savedProductLine, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateProductLine(@PathVariable("id") int id,
            @Valid @RequestBody CProductLine cProductLine) {
        try {
            CProductLine savedProductLine = productLineService.updateProductLine(id,
                    cProductLine);
            return new ResponseEntity<>(savedProductLine, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteProductById(@PathVariable int id) {
        try {
            productLineService.deleteProductLine(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
