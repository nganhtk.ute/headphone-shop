package devcamp.shop.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.CCustomer;
import devcamp.shop.model.CEmployee;
import devcamp.shop.model.COrder;
import devcamp.shop.model.CProduct;
import devcamp.shop.repository.IOrderRepository;
import devcamp.shop.service.ExcelExporter;
import devcamp.shop.service.OrderService;

@CrossOrigin
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    OrderService orderService;

    @Autowired
    IOrderRepository orderRepository;

    @GetMapping("/{id}")
    public ResponseEntity<COrder> getOrderByOrderId(@PathVariable("id") int orderId) {
        try {
            COrder findOrder = orderService.getOrderById(orderId);
            return new ResponseEntity<>(findOrder, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<COrder>> getOrder() {
        try {
            List<COrder> lstOrders = orderService.getOrders();

            return new ResponseEntity<>(lstOrders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging")
    public ResponseEntity<Object> findWithPaging(
            @RequestParam(value = "customer", required = false) Optional<Integer> customer,
            @RequestParam(value = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate,
            @RequestParam(value = "keyword", required = false) Optional<String> keyword,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "6") int size) {
        try {
            return new ResponseEntity<>(orderRepository.findOrderByRequest(
                    customer,
                    keyword,
                    startDate,
                    endDate,
                    PageRequest.of(page, size)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CUSTOMER')")
    public ResponseEntity<Object> createOrder(@Valid @RequestBody COrder COrder) {
        try {
            COrder savedOrder = orderService.createOrder(COrder);
            return new ResponseEntity<>(savedOrder, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_CUSTOMER')")
    public ResponseEntity<Object> updateOrder(@PathVariable("id") int id, @Valid @RequestBody COrder cOrder) {
        try {
            COrder savedOrder = orderService.updateOrder(id,
                    cOrder);
            return new ResponseEntity<>(savedOrder, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteOrderById(@PathVariable int id) {
        try {
            orderService.deleteOrder(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 
     * @param startDate
     * @param endDate
     * @param type
     *                  1: doanh thu theo ngay
     *                  2: doanh thu theo ngay trong tuan
     *                  3: doanh thu theo ngay trong thang
     *                  4: doanh thu theo tuan trong thang
     *                  5: doanh thu theo thang trong nam
     * @return
     */
    @GetMapping("/report")
    public ResponseEntity<Object> reportOrders(
            @RequestParam(value = "startDate", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date startDate,
            @RequestParam(value = "endDate", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate,
            @RequestParam(value = "type", required = true) Integer type) {
        try {
            List<COrder.Report> reportList = orderRepository.reportOrders(startDate, endDate, type);
            return new ResponseEntity<>(reportList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/export/excel")
    public void exportToExcel(HttpServletResponse response, @RequestParam(value = "min", required = false) Integer min,
            @RequestParam(value = "customer", required = false) Optional<Integer> customer,
            @RequestParam(value = "keyword", required = false) Optional<String> keyword,
            @RequestParam(value = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date startDate,
            @RequestParam(value = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endDate,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "6") int size) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_" + currentDateTime +
                ".xlsx";
        response.setHeader(headerKey, headerValue);

        Page<COrder> orderList = orderRepository.findOrderByRequest(
                customer, keyword, startDate, endDate, PageRequest.of(page, size));
        List<COrder> orders = orderList.getContent();

        List<String> headers = new ArrayList<String>(
                List.of("Customer name", "Receiver Name", "Receiver Phone", "Receiver Address", "Note",
                        "Payment Method", "Order Date", "Shipped Date", "status"));

        List<String> columns = new ArrayList<String>(
                List.of("getCustomerName", "getReceiverName", "getReceiverPhone",
                        "getReceiverAddress", "getNote", "getPaymentMT", "getOrderDateString", "getShippedDateString",
                        "getStatusString"));

        ExcelExporter<COrder> excelExporter = new ExcelExporter<COrder>(orders, headers, columns, "Orders");
        excelExporter.export(response);
    }
}
