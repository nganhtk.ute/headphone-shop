package devcamp.shop.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.CProduct;
import devcamp.shop.repository.IProductRepository;
import devcamp.shop.service.ProductService;

@CrossOrigin
@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @Autowired
    IProductRepository productRepository;

    @GetMapping("/{id}")
    public ResponseEntity<CProduct> getProjectByProjectId(@PathVariable("id") int productId) {
        try {
            CProduct findProject = productService.getProductById(productId);
            return new ResponseEntity<>(findProject, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<CProduct>> getProduct() {
        try {
            List<CProduct> lstProducts = productService.getProducts();

            return new ResponseEntity<>(lstProducts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging")
    public ResponseEntity<Page<CProduct>> getProductPaging() {
        try {
            Page<CProduct> page = productRepository.findAll(PageRequest.of(0, 8));
            return new ResponseEntity<>(page, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging1")
    public ResponseEntity<Object> findWithPaging(
            @RequestParam(value = "brand", required = false) Optional<Integer> brand,
            @RequestParam(value = "priceMin", required = false) Optional<Double> priceMin,
            @RequestParam(value = "priceMax", required = false) Optional<Double> priceMax,
            @RequestParam(value = "category", required = false) Optional<Integer> category,
            @RequestParam(value = "color", required = false) Optional<Integer> color,
            @RequestParam(value = "keyword", required = false) Optional<String> keyword,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "6") int size) {
        try {
            return new ResponseEntity<>(productRepository.findProductByRequest(
                    brand,
                    priceMin,
                    priceMax,
                    category,
                    color,
                    keyword,
                    PageRequest.of(page, size)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createProduct(@Valid @RequestBody CProduct CProduct) {
        try {
            CProduct savedProduct = productService.createProduct(CProduct);
            return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateProduct(@PathVariable("id") int id, @Valid @RequestBody CProduct CProduct) {
        try {
            CProduct savedProduct = productService.updateProduct(id,
                    CProduct);
            return new ResponseEntity<>(savedProduct, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteProductById(@PathVariable int id) {
        try {
            productService.deleteProduct(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
