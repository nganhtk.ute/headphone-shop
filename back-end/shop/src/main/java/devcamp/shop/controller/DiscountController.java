package devcamp.shop.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.CDiscount;
import devcamp.shop.repository.IDiscountRepository;
import devcamp.shop.service.DiscountService;

@CrossOrigin
@RestController
@RequestMapping("/discount")
public class DiscountController {
    @Autowired
    DiscountService productService;

    @Autowired
    IDiscountRepository productRepository;

    @GetMapping("/{id}")
    public ResponseEntity<CDiscount> getDiscountByDiscountId(@PathVariable("id") int productId) {
        try {
            CDiscount findDiscount = productService.getDiscountById(productId);
            return new ResponseEntity<>(findDiscount, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<CDiscount>> getDiscount() {
        try {
            List<CDiscount> lstDiscounts = productService.getDiscounts();

            return new ResponseEntity<>(lstDiscounts, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging")
    public ResponseEntity<Page<CDiscount>> getDiscountPaging() {
        try {
            Page<CDiscount> page = productRepository.findAll(PageRequest.of(0, 8));
            return new ResponseEntity<>(page, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/find")
    public ResponseEntity<Object> findDiscount(
            @RequestParam(value = "keyword", required = false) Optional<String> keyword) {
        try {
            return new ResponseEntity<>(productRepository.findDiscountByKeyword(
                    keyword), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging1")
    public ResponseEntity<Object> findWithPaging(
            @RequestParam(value = "keyword", required = false) Optional<String> keyword,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "6") int size) {
        try {
            return new ResponseEntity<>(productRepository.findDiscountByRequest(
                    keyword,
                    PageRequest.of(page, size)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createDiscount(@Valid @RequestBody CDiscount CDiscount) {
        try {
            CDiscount savedDiscount = productService.createDiscount(CDiscount);
            return new ResponseEntity<>(savedDiscount, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateDiscount(@PathVariable("id") int id, @Valid @RequestBody CDiscount CDiscount) {
        try {
            CDiscount savedDiscount = productService.updateDiscount(id,
                    CDiscount);
            return new ResponseEntity<>(savedDiscount, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteDiscountById(@PathVariable int id) {
        try {
            productService.deleteDiscount(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
