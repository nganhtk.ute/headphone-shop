package devcamp.shop.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import devcamp.shop.model.CFile;
import devcamp.shop.model.FileUploadUtil;
import devcamp.shop.service.FileService;

@CrossOrigin
@RestController
@RequestMapping("/photos")
public class UploadController {

    @Autowired
    FileService fileService;

    @PostMapping("/")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<CFile>> uploadPhotos(@RequestParam("photos") MultipartFile[] photos)
            throws IOException {
        try {
            List<CFile> cFiles = new ArrayList<CFile>();

            for (MultipartFile photo : photos) {
                String fileName = StringUtils.cleanPath(UUID.randomUUID() + "-" + photo.getOriginalFilename());
                String uploadDir = "photos/";
                FileUploadUtil.saveFile(uploadDir, fileName, photo);

                CFile newFile = new CFile();
                newFile.setName(fileName);
                newFile.setOriginalName(photo.getOriginalFilename());

                CFile savedFile = fileService.createFile(newFile);
                cFiles.add(savedFile);
            }

            return new ResponseEntity<>(cFiles, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
