package devcamp.shop.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.CCustomer;
import devcamp.shop.model.CUser;
import devcamp.shop.repository.ICustomerRepository;
import devcamp.shop.repository.IUserRepository;
import devcamp.shop.service.CustomerService;
import devcamp.shop.service.ExcelExporter;

@CrossOrigin
@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @Autowired
    ICustomerRepository customerRepository;

    @Autowired
    IUserRepository userRepository;

    @GetMapping("/{id}")
    public ResponseEntity<CCustomer> getCustomerByCustomerId(
            @PathVariable("id") int customerId) {
        try {
            CCustomer findCustomer = customerService.getCustomerById(customerId);
            return new ResponseEntity<>(findCustomer, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<CCustomer>> getCustomers() {
        try {
            List<CCustomer> lstCustomer = customerService.getCustomers();
            return new ResponseEntity<>(lstCustomer, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/pageable")
    public ResponseEntity<List<CCustomer>> getFiveCustomer(
            @RequestParam(value = "page", defaultValue = "1") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<CCustomer> list = new ArrayList<CCustomer>();
            customerRepository.findAll(pageWithFiveElements).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> createCustomer(
            @Validated(CCustomer.ValidationCreate.class) @RequestBody CCustomer cCustomer) {
        try {
            CCustomer savedCustomer = customerService.createCustomer(cCustomer);
            return new ResponseEntity<>(savedCustomer, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") int id, @Valid @RequestBody CCustomer cCustomer) {
        try {
            CCustomer savedCustomer = customerService.updateCustomer(id, cCustomer);
            return new ResponseEntity<>(savedCustomer, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/me")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Object> updateMyProfile(@Valid @RequestBody CCustomer cCustomer,
            @AuthenticationPrincipal CUser currentLoggedInUser) {
        try {
            CCustomer savedCustomer = customerService.updateCustomer(currentLoggedInUser.getId(), cCustomer);
            return new ResponseEntity<>(savedCustomer, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable int id) {
        try {
            customerService.deleteCustomer(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/report")
    public ResponseEntity<Object> reportCustomers(
            @RequestParam(value = "min", required = false) Integer min,
            @RequestParam(value = "max", required = false) Integer max,
            @RequestParam(value = "minOrder", required = false) Integer minOrder,
            @RequestParam(value = "maxOrder", required = false) Integer maxOrder,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "6") int size) {
        try {
            return new ResponseEntity<>(customerRepository.reportCustomers(
                    min, max, minOrder, maxOrder, PageRequest.of(page, size)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/export/excel")
    public void exportToExcel(HttpServletResponse response, @RequestParam(value = "min", required = false) Integer min,
            @RequestParam(value = "max", required = false) Integer max,
            @RequestParam(value = "minOrder", required = false) Integer minOrder,
            @RequestParam(value = "maxOrder", required = false) Integer maxOrder,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "6") int size) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);

        Page<CCustomer> customerList = customerRepository.reportCustomers(
                min, max, minOrder, maxOrder, PageRequest.of(page, size));
        List<CCustomer> customers = customerList.getContent();

        List<String> headers = new ArrayList<String>(
                List.of("Fullname", "Phone number", "Email", "Address", "Total Payments ($)", "Total Orders"));

        List<String> columns = new ArrayList<String>(
                List.of("getFullname", "getPhone", "getEmail", "getAddress", "getTotalOrderPayment", "getCountOrder"));

        ExcelExporter<CCustomer> excelExporter = new ExcelExporter<CCustomer>(customers, headers, columns, "Customers");
        excelExporter.export(response);
    }
}
