package devcamp.shop.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import devcamp.shop.model.CReview;
import devcamp.shop.repository.IReviewRepository;
import devcamp.shop.service.ReviewService;

@CrossOrigin
@RestController
@RequestMapping("/review")
public class ReviewController {
    @Autowired
    ReviewService reviewService;

    @Autowired
    IReviewRepository reviewRepository;

    @GetMapping("/{id}")
    public ResponseEntity<CReview> getProjectByProjectId(@PathVariable("id") int reviewId) {
        try {
            CReview findReview = reviewService.getReviewById(reviewId);
            return new ResponseEntity<>(findReview, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<CReview>> getReview() {
        try {
            List<CReview> lstReviews = reviewService.getReviews();

            return new ResponseEntity<>(lstReviews, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/review-list")
    public ResponseEntity<Object> findWithoutPaging(
            @RequestParam(value = "product", required = false) Optional<Integer> product) {
        try {
            return new ResponseEntity<>(reviewRepository.findReviewByProductId(
                    product), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/review-list-text")
    public ResponseEntity<Object> findReviewTextWithPaging(
            @RequestParam(value = "product", required = false) Optional<Integer> product,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "2") int size) {
        try {
            return new ResponseEntity<>(
                    reviewRepository.findReviewByReviewText(product, PageRequest.of(page, size)),
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging1")
    public ResponseEntity<Object> findWithPaging(
            @RequestParam(value = "product", required = false) Optional<Integer> product,
            @RequestParam(value = "rate", required = false) Optional<Integer> rate,
            @RequestParam(value = "customer", required = false) Optional<String> customer,
            @RequestParam(value = "orderID", required = false) Optional<Integer> orderID,
            @RequestParam(value = "keyword", required = true) String keyword,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "8") int size) {
        try {
            return new ResponseEntity<>(reviewRepository.findReviewByRequest(
                    product,
                    rate,
                    customer,
                    orderID,
                    keyword,
                    PageRequest.of(page, size)), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/paging")
    public ResponseEntity<Page<CReview>> getReviewPaging() {
        try {
            Page<CReview> page = reviewRepository.findAll(PageRequest.of(0, 2));
            return new ResponseEntity<>(page, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_CUSTOMER')")
    public ResponseEntity<Object> createReview(@Valid @RequestBody CReview CReview) {
        try {

            CReview savedReview = reviewService.createReview(CReview);
            return new ResponseEntity<>(savedReview, HttpStatus.CREATED);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> updateReview(@PathVariable("id") int id, @Valid @RequestBody CReview CReview) {
        try {
            CReview savedReview = reviewService.updateReview(id,
                    CReview);
            return new ResponseEntity<>(savedReview, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Object> deleteReviewById(@PathVariable int id) {
        try {
            reviewService.deleteReview(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
