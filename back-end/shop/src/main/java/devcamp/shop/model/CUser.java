package devcamp.shop.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "user")
public class CUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Chưa nhập username", groups = { ValidationCreate.class })
    // @Size(min = 2, message = "Username tối thiểu 2 ký tự")
    @Column(name = "username", unique = true, updatable = false)
    private String username;

    // @NotNull(message = "Chưa nhập password")
    // @Size(min = 2, message = "Password tối thiểu 2 ký tự")
    @Column(name = "password")
    private String password;

    public CUser(int id,
            @NotNull(message = "Chưa nhập username") @Size(min = 2, message = "Username tối thiểu 2 ký tự") String username,
            @NotNull(message = "Chưa nhập password") @Size(min = 2, message = "Password tối thiểu 2 ký tự") String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    public CUser() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public interface ValidationCreate {
        // validation group marker interface
    }
}
