package devcamp.shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "street")

public class CStreet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nhập đường")
    @Column(name = "_prefix", nullable = false)
    private String prefix;

    @NotNull(message = "Nhập tên đường")
    @Size(min = 1, message = "Tên đường tối thiểu 1 ký tự")
    @Column(name = "_name", nullable = false)
    private String streetName;

    @ManyToOne
    @JoinColumn(name = "_district_id")
    private CDistrict district;

    @ManyToOne
    @JoinColumn(name = "_province_id")
    private CProvince province;

    public CStreet() {
    }

    public CStreet(int id, String streetName, String prefix, CDistrict district) {
        this.id = id;
        this.streetName = streetName;
        this.prefix = prefix;
        this.district = district;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }
}
