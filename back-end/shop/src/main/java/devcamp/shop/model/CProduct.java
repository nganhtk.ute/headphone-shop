package devcamp.shop.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "product")
public class CProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Chưa nhập mã sản phẩm")
    @Size(min = 2, message = "Mã sản phẩm tối thiểu 2 ký tự")
    @Column(name = "product_code", unique = true)
    private String productCode;

    @NotNull(message = "Chưa nhập tên sản phẩm")
    @Size(min = 2, message = "Tên sản phẩm tối thiểu 2 ký tự")
    @Column(name = "product_name")
    private String productName;

    @NotNull(message = "Chưa nhập mô tả sản phẩm")
    @Column(name = "product_description")
    private String productDescription;

    @NotNull(message = "Chưa nhập thông tin bảo hành")
    @Column(name = "product_guarantee")
    private String productGuarantee;

    @Column(name = "quantity_in_stock")
    private int quantityInStock;

    @Column(name = "original_price")
    private Double originalPrice;

    @NotNull(message = "Chưa nhập giá bán")
    @Column(name = "sell_price")
    private Double sellPrice;

    @ManyToOne
    @JoinColumn(name = "brand")
    private CProductLine brand;

    @ManyToOne
    @JoinColumn(name = "category")
    private CProductLine category;

    @ManyToOne
    @JoinColumn(name = "color")
    private CProductLine color;

    @OneToMany
    @JoinTable(name = "product_files", joinColumns = @JoinColumn(name = "product_id"), inverseJoinColumns = @JoinColumn(name = "file_id"))
    private Set<CFile> photos = new HashSet<CFile>();

    public CProduct() {
    }

    public CProduct(int id,
            @NotNull(message = "Chưa nhập mã sản phẩm") @Size(min = 2, message = "Mã sản phẩm tối thiểu 2 ký tự") String productCode,
            @NotNull(message = "Chưa nhập tên sản phẩm") @Size(min = 2, message = "Tên sản phẩm tối thiểu 2 ký tự") String productName,
            String productDescription, String productGuarantee, int quantityInStock, Double originalPrice,
            @NotNull(message = "Chưa nhập giá bán") Double sellPrice, CProductLine brand,
            CProductLine category, CProductLine color, Set<CFile> photos) {
        this.id = id;
        this.productCode = productCode;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productGuarantee = productGuarantee;
        this.quantityInStock = quantityInStock;
        this.originalPrice = originalPrice;
        this.sellPrice = sellPrice;
        this.brand = brand;
        this.category = category;
        this.color = color;
        this.photos = photos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductGuarantee() {
        return productGuarantee;
    }

    public void setProductGuarantee(String productGuarantee) {
        this.productGuarantee = productGuarantee;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public Double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(Double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public CProductLine getBrand() {
        return brand;
    }

    public void setBrand(CProductLine brand) {
        this.brand = brand;
    }

    public CProductLine getCategory() {
        return category;
    }

    public void setCategory(CProductLine category) {
        this.category = category;
    }

    public CProductLine getColor() {
        return color;
    }

    public void setColor(CProductLine color) {
        this.color = color;
    }

    public Set<CFile> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<CFile> photos) {
        this.photos = photos;
    }

}
