package devcamp.shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "order_detail")
public class COrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "product_id", updatable = false)
    private CProduct product;

    @ManyToOne
    @JoinColumn(name = "order_id", updatable = false)
    private COrder order;

    @NotNull(message = "Chưa có số lượng")
    @Min(value = 1, message = "Chưa có số lượng")
    @Column(name = "quantity")
    private int quantity = 0;

    @NotNull(message = "Chưa có giá bán")
    @Min(value = 0, message = "Chưa có giá bán")
    @Column(name = "sell_price")
    private Double sellPrice = 0.0;

    @Column(name = "reviewStatus")
    private Boolean reviewStatus;

    public COrderDetail(int id, CProduct product, COrder order,
            @NotNull(message = "Chưa có số lượng") @Min(value = 1, message = "Chưa có số lượng") int quantity,
            @NotNull(message = "Chưa có giá bán") @Min(value = 0, message = "Chưa có giá bán") Double sellPrice,
            Boolean reviewStatus) {
        this.id = id;
        this.product = product;
        this.order = order;
        this.quantity = quantity;
        this.sellPrice = sellPrice;
        this.reviewStatus = reviewStatus;
    }

    public int getId() {
        return id;
    }

    public COrderDetail() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public CProduct getProduct() {
        return product;
    }

    public void setProduct(CProduct product) {
        this.product = product;
    }

    public COrder getOrder() {
        return order;
    }

    public void setOrder(COrder order) {
        this.order = order;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Boolean isReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(Boolean reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

}
