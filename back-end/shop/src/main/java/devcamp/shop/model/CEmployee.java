package devcamp.shop.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.data.annotation.Transient;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "employee")
public class CEmployee extends CUser {
    @NotNull(message = "Chưa nhập họ tên")
    @Column(name = "fullname")
    private String fullname;

    @NotNull(message = "Chưa nhập số điện thoại")
    @Column(name = "phone")
    private String phone;

    @NotNull(message = "Chưa nhập email")
    @Column(name = "email", unique = true)
    private String email;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private CProvince province;

    @ManyToOne
    @JoinColumn(name = "district_id")
    @JsonIgnoreProperties("province")
    private CDistrict district;

    @ManyToOne
    @JoinColumn(name = "ward_id")
    @JsonIgnoreProperties("district")
    private CWard ward;

    @ManyToOne
    @JoinColumn(name = "street_id")
    @JsonIgnoreProperties("district")
    private CStreet street;

    @Transient
    public String getAddress() {
        String address = "";

        if (this.province == null || this.district == null || this.ward == null || this.ward == null) {
            address = "";
        } else {
            address = this.street.getStreetName() + ", " + this.ward.getPrefix() + " " + this.ward.getWardName()
                    + ", " + this.district.getPrefix() + " " +
                    this.district.getDistrictName()
                    + ", " + this.province.getProvinceName();
        }

        return address;

    }

    @Column(name = "job_title")
    private String jobTitle;

    @ManyToOne
    @JoinColumn(name = "ReportsTo")
    @JsonIgnoreProperties("reportsTo")
    private CEmployee reportsTo;

    @Column(name = "BirthDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;

    @Column(name = "HireDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date hireDate;

    @OneToMany
    @JoinTable(name = "employee_files", joinColumns = @JoinColumn(name = "employee_id"), inverseJoinColumns = @JoinColumn(name = "file_id"))
    private Set<CFile> photos = new HashSet<CFile>();

    public CEmployee() {
    }

    public CEmployee(int id,
            @NotNull(message = "Chưa nhập username") @Size(min = 2, message = "Username tối thiểu 2 ký tự") String username,
            @NotNull(message = "Chưa nhập password") @Size(min = 2, message = "Password tối thiểu 2 ký tự") String password,
            @NotNull(message = "Chưa nhập họ tên") String fullname,
            @NotNull(message = "Chưa nhập số điện thoại") String phone,
            @NotNull(message = "Chưa nhập email") String email, CProvince province, CDistrict district, CWard ward,
            CStreet street, String jobTitle, CEmployee reportsTo, Date birthDate, Date hireDate, Set<CFile> photos) {
        super(id, username, password);
        this.fullname = fullname;
        this.phone = phone;
        this.email = email;
        this.province = province;
        this.district = district;
        this.ward = ward;
        this.street = street;
        this.jobTitle = jobTitle;
        this.reportsTo = reportsTo;
        this.birthDate = birthDate;
        this.hireDate = hireDate;
        this.photos = photos;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    public CWard getWard() {
        return ward;
    }

    public void setWard(CWard ward) {
        this.ward = ward;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public CEmployee getReportsTo() {
        return reportsTo;
    }

    public void setReportsTo(CEmployee reportsTo) {
        this.reportsTo = reportsTo;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public Set<CFile> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<CFile> photos) {
        this.photos = photos;
    }

    public CStreet getStreet() {
        return street;
    }

    public void setStreet(CStreet street) {
        this.street = street;
    }
}
