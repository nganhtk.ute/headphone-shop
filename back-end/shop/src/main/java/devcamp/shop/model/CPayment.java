package devcamp.shop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payment")
public class CPayment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    @JoinColumn(name = "order_id")
    private COrder order;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private CCustomer customer;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "type", columnDefinition = "ENUM('cash', 'card')")
    @Enumerated(EnumType.STRING)
    private PaymentType type;

    public enum PaymentType {
        cash, card
    }

    public CPayment() {
    }

    public CPayment(int id, COrder order, CCustomer customer, Double amount, PaymentType type) {
        this.id = id;
        this.order = order;
        this.customer = customer;
        this.amount = amount;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public COrder getOrder() {
        return order;
    }

    public void setOrder(COrder order) {
        this.order = order;
    }

    public CCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(CCustomer customer) {
        this.customer = customer;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public PaymentType getType() {
        return type;
    }

    public void setType(PaymentType type) {
        this.type = type;
    }

}
