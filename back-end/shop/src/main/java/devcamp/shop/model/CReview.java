package devcamp.shop.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "review")
public class CReview {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JoinColumn(name = "product_id")
    private int product;

    @Column(name = "customer")
    private String customer;

    @Column(name = "review_text")
    private String reviewText;

    @Column(name = "orderID")
    private int orderID;

    @NotNull(message = "Chưa nhập số rate")
    @Column(name = "rate")
    private Integer rate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createDate")
    @CreatedDate
    private Date createDate;

    @OneToMany
    @JoinTable(name = "review_files", joinColumns = @JoinColumn(name = "review_id"), inverseJoinColumns = @JoinColumn(name = "file_id"))
    private Set<CFile> photos = new HashSet<CFile>();

    public CReview() {
    }

    public CReview(int id, int product, String customer, String reviewText, int orderID, Integer rate, Date createDate,
            Set<CFile> photos) {
        this.id = id;
        this.product = product;
        this.customer = customer;
        this.reviewText = reviewText;
        this.orderID = orderID;
        this.rate = rate;
        this.createDate = createDate;
        this.photos = photos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Set<CFile> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<CFile> photos) {
        this.photos = photos;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

}
