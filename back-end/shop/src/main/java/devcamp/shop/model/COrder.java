package devcamp.shop.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.BatchSize;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "orders")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "note")
    private String note;

    @NotNull(message = "Chưa nhập tên người nhập")
    @Column(name = "receiver_name")
    private String receiverName;

    @NotNull(message = "Chưa nhập số điện thoại người nhập")
    @Column(name = "receiver_phone")
    private String receiverPhone;

    @Column(name = "payment_method", columnDefinition = "ENUM('COD', 'card')")
    @Enumerated(EnumType.STRING)
    private paymentMethod paymentMethod;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    @JsonIgnoreProperties(value = { "orderList" })
    private CCustomer customer;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "order_date")
    @CreatedDate
    private Date orderDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "shipped_date")
    private Date shippedDate;

    @Column(name = "status", columnDefinition = "ENUM('paid', 'unpaid')")
    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = { "order" }, allowSetters = true)
    private Set<COrderDetail> orderDetails = new HashSet<COrderDetail>();

    @ManyToOne
    @JoinColumn(name = "province_id", nullable = false)
    private CProvince province;

    @ManyToOne
    @JoinColumn(name = "district_id", nullable = false)
    @JsonIgnoreProperties("province")
    private CDistrict district;

    @ManyToOne
    @JoinColumn(name = "ward_id", nullable = false)
    @JsonIgnoreProperties("district")
    private CWard ward;

    @ManyToOne
    @JoinColumn(name = "street_id", nullable = false)
    @JsonIgnoreProperties("district")
    private CStreet street;

    @Transient
    public String getOrderDateString() {
        return String.valueOf(this.orderDate);
    }

    @Transient
    public String getShippedDateString() {
        return String.valueOf(this.shippedDate);
    }

    @Transient
    public String getStatusString() {
        return String.valueOf(this.status);
    }

    @Transient
    public String getReceiverAddress() {
        String receiverAddress = "";

        if (this.province == null || this.district == null || this.ward == null || this.ward == null) {
            receiverAddress = "";
        } else {
            receiverAddress = this.street.getStreetName() + ", " + this.ward.getPrefix() + " " + this.ward.getWardName()
                    + ", " + this.district.getPrefix() + " " +
                    this.district.getDistrictName()
                    + ", " + this.province.getProvinceName();
        }

        return receiverAddress;

    }

    public enum Status {
        paid, unpaid
    }

    public enum paymentMethod {
        COD, card
    }

    public COrder(int id, String note, String receiverName, String receiverPhone,
            devcamp.shop.model.COrder.paymentMethod paymentMethod, CCustomer customer, Date orderDate, Date shippedDate,
            Status status, Set<COrderDetail> orderDetails, CProvince province, CDistrict district, CWard ward,
            CStreet street) {
        this.id = id;
        this.note = note;
        this.receiverName = receiverName;
        this.receiverPhone = receiverPhone;
        this.paymentMethod = paymentMethod;
        this.customer = customer;
        this.orderDate = orderDate;
        this.shippedDate = shippedDate;
        this.status = status;
        this.orderDetails = orderDetails;
        this.province = province;
        this.district = district;
        this.ward = ward;
        this.street = street;
    }

    public COrder() {
    }

    public int getId() {
        return id;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public void setId(int id) {
        this.id = id;
    }

    public CCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(CCustomer customer) {
        this.customer = customer;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(Date shippedDate) {
        this.shippedDate = shippedDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Set<COrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void removeAllOrderDetails() {
        this.orderDetails.clear();
    }

    public void setOrderDetails(Set<COrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public void addOrderDetail(COrderDetail orderDetail) {
        orderDetail.setOrder(this);
        this.orderDetails.add(orderDetail);
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public paymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    @Transient
    public String getPaymentMT() {
        return String.valueOf(this.paymentMethod);
    }

    @Transient
    public String getCustomerName() {
        return String.valueOf(getCustomer().getFullname());
    }

    public void setPaymentMethod(paymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    public CWard getWard() {
        return ward;
    }

    public void setWard(CWard ward) {
        this.ward = ward;
    }

    public CStreet getStreet() {
        return street;
    }

    public void setStreet(CStreet street) {
        this.street = street;
    }

    public interface Report {
        Date getDate();

        Integer getWeek();

        String getDayOfWeek();

        String getMonth();

        Integer getDayOfMonth();

        Double getRevenue();
    }

    public interface ReportCustomer {
        Integer getId();

        String getPhone();

        String getEmail();

        String getFullname();

        Double getTotalPayment();

    }
}
