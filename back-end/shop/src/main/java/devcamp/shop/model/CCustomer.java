package devcamp.shop.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "customer")
public class CCustomer extends CUser {

    @Column(name = "fullname")
    private String fullname;

    @NotNull(message = "Chưa nhập số điện thoại")
    @Column(name = "phone", unique = true)
    private String phone;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private CProvince province;

    @ManyToOne
    @JoinColumn(name = "district_id")
    @JsonIgnoreProperties("province")
    private CDistrict district;

    @ManyToOne
    @JoinColumn(name = "ward_id")
    @JsonIgnoreProperties("district")
    private CWard ward;

    @ManyToOne
    @JoinColumn(name = "street_id")
    @JsonIgnoreProperties("district")
    private CStreet street;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    // @JsonIgnoreProperties(value = { "" }, allowSetters = true)
    // @JsonBackReference
    @JsonIgnore
    private Set<CReview> reviews = new HashSet<CReview>();

    @Transient
    public String getAddress() {
        String address = "";

        if (this.province == null || this.district == null || this.ward == null || this.ward == null) {
            address = "";
        } else {
            address = this.street.getStreetName() + ", " + this.ward.getPrefix() + " " + this.ward.getWardName()
                    + ", " + this.district.getPrefix() + " " +
                    this.district.getDistrictName()
                    + ", " + this.province.getProvinceName();
        }

        return address;

    }

    @Transient
    public String getCountOrder() {
        String result = String.valueOf(orderList.size());
        return result;
    }

    @Transient
    public String getTotalOrderPayment() {
        Double result = 0.0;

        for (COrder order : orderList) {
            Set<COrderDetail> orderDetails = order.getOrderDetails();
            Double total = 0.0;
            for (COrderDetail orderDetail : orderDetails) {
                total = total + orderDetail.getQuantity() * orderDetail.getSellPrice();
            }
            result = result + total;
        }

        return String.valueOf(result);
    }

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = { "customer" }, allowSetters = true)
    private Set<COrder> orderList = new HashSet<COrder>();

    public CCustomer(int id,
            @NotNull(message = "Chưa nhập username") @Size(min = 2, message = "Username tối thiểu 2 ký tự") String username,
            @NotNull(message = "Chưa nhập password") @Size(min = 2, message = "Password tối thiểu 2 ký tự") String password,
            String fullname, @NotNull(message = "Chưa nhập số điện thoại") String phone, String email,
            CProvince province, CDistrict district, CWard ward, CStreet street,
            Set<COrder> orderList) {
        super(id, username, password);
        this.fullname = fullname;
        this.phone = phone;
        this.email = email;
        this.province = province;
        this.district = district;
        this.ward = ward;
        this.street = street;

        this.orderList = orderList;
    }

    public CCustomer() {
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    public CDistrict getDistrict() {
        return district;
    }

    public void setDistrict(CDistrict district) {
        this.district = district;
    }

    public CWard getWard() {
        return ward;
    }

    public void setWard(CWard ward) {
        this.ward = ward;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<COrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(Set<COrder> orderList) {
        this.orderList.clear();
        this.orderList.addAll(orderList);
    }

    public CStreet getStreet() {
        return street;
    }

    public void setStreet(CStreet street) {
        this.street = street;
    }

}
