package devcamp.shop.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "discount")
public class CDiscount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Chưa nhập mã discount")
    @Size(min = 2, message = "Mã discount tối thiểu 2 ký tự")
    @Column(name = "discount_code", unique = true)
    private String discountCode;

    @Column(name = "discount_description")
    private String discountDescription;

    @NotNull(message = "Chưa nhập số phần trăm giảm")
    @Column(name = "percent")
    private int percent;

    public CDiscount() {
    }

    public CDiscount(int id,
            @NotNull(message = "Chưa nhập mã discount") @Size(min = 2, message = "Mã discount tối thiểu 2 ký tự") String discountCode,
            String discountDescription, @NotNull(message = "Chưa nhập số phần trăm giảm") int percent) {
        this.id = id;
        this.discountCode = discountCode;
        this.discountDescription = discountDescription;
        this.percent = percent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDiscountCode() {
        return discountCode;
    }

    public void setDiscountCode(String discountCode) {
        this.discountCode = discountCode;
    }

    public String getDiscountDescription() {
        return discountDescription;
    }

    public void setDiscountDescription(String discountDescription) {
        this.discountDescription = discountDescription;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

}
